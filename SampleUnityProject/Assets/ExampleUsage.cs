﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using ARGlass;
using hnine.arglass;

public class ExampleUsage : MonoBehaviour
{
    Text m_InitDebugText;
    Text m_SLAMDebugText;
    Text m_GazeDebugText;
    Text m_GestureDebugText;

    bool m_CameraPreviewReady = false;

    public GameObject MusicPlayerPrefab;
    public GameObject GalleryPrefab;
    public GameObject CloudPrefab;

    public string CoSLAMIpAddress = "192.168.1.3";
    public int CoSLAMPort = 6666;

    public Camera FirstPersonCamera;
    public GameObject AnchorPrefab;
    public GameObject HitObject { get; private set; }
    private GameObject MusicPlayer = null;
    private GameObject Gallery = null;
    private GameObject Cloud = null;
    private GameObject PlaneVisualizer = null;

    private Anchor[] GuideAnchors = { null, null, null, null };
    private GameObject[] AnchorMarkers = { null, null, null, null };
    private Vector3[] m_LocalAnchorPoisitions = { new Vector3(), new Vector3(), new Vector3(), new Vector3() };
    private Quaternion[] m_LocalAnchorRotations = { new Quaternion(), new Quaternion(), new Quaternion(), new Quaternion() };
    private int m_PlacedGuideAnchors = 0;
    private bool m_WidgetsArePlaced = false;
    private float WidgetScale = 0.9f;
    private GameObject Aim = null;

    // Start is called before the first frame update
    void Start()
    {
        m_InitDebugText = GameObject.Find("InitDebugText").GetComponent<Text>();
        m_SLAMDebugText = GameObject.Find("SLAMDebugText").GetComponent<Text>();
        m_GazeDebugText = GameObject.Find("GazeDebugText").GetComponent<Text>();
        m_GestureDebugText = GameObject.Find("GestureDebugText").GetComponent<Text>();

        Aim = GameObject.Find("Aim");
        Aim.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("Delta: " + Time.deltaTime + " / " + Frame.Pose.position);
        
        if (ARGlassController.Instance.IsInitialized())
        {
            // SLAM
            m_SLAMDebugText.text = "Position: " + Frame.Pose.position + "\nRotation: " + Frame.Pose.rotation.eulerAngles;

            // Gaze tracking
            //m_GazeDebugText.text = "Gaze: " + string.Join(" / ", Frame.GazeData);
            DetectedPlane plane = (DetectedPlane)Frame.Trackables[0];
            m_GazeDebugText.text = "Plane Normal: " + plane.Normal.x + ", " + plane.Normal.y + ", " + plane.Normal.z + "\nPosition: " + plane.Center.x + ", " + plane.Center.y + ", " + plane.Center.z;

            // Hand gesture
            m_GestureDebugText.text = "Left hand: " + Frame.HandGesture.LeftHandGesture + "\nRight hand: " + Frame.HandGesture.RightHandGesture;
            //m_GestureDebugText.text = "Fusion state: " + ARGlassController.Instance.GetCoSLAMFusionState() + "\nSequence ID: " + ARGlassController.Instance.GetCoSLAMSequenceId();

            if (!GuideAnchorsArePlaced())
            {
                // Simple example code to place a gameobject on the touched position
                if (Input.touchCount > 0)
                {
                    Touch touch = Input.GetTouch(0);                
                    if (touch.phase == TouchPhase.Began)
                    {
                        Vector2 pos = touch.position;
                        var hit = Frame.RaycastAll((int)pos.x, (int)pos.y);
                        if(hit.Count > 0)
                        {
                            /*
                            if(Frame.GlobalAnchors.Count < 1)
                            {                            
                                GameObject anchorObj = new GameObject();
                                GlobalAnchor globalAnchor = anchorObj.AddComponent<GlobalAnchor>();
                                Frame.GlobalAnchors.Add(globalAnchor);                          
                            }                        
                            GameObject obj = GameObject.Instantiate(GameObject.Find("Cube"), hit[0].Pose.position, hit[0].Pose.rotation);
                            obj.transform.localScale *= 0.1f;
                            Frame.GlobalAnchors[0].AddChildObject(obj);
                            */
                            SpawnGuideAnchor(hit[0]);
                        }
                    }
                }

                if (GuideAnchorsArePlaced())
                {
                    PlaneVisualizer = GameObject.Find("PlaneVisualizer");
                    PlaneVisualizer.SetActive(false);
                    Aim.SetActive(true);
                }

                return;
            }

            SetAimFromGazing();

            if (!WidgetsArePlaced())
            {
                _DestroyAnchorMarkers();

                SpawnWidgets(m_LocalAnchorPoisitions, m_LocalAnchorRotations);
            }
            else
            {
                RecognizeGazing();            
            }
        }
    }

    public bool GuideAnchorsArePlaced()
    {
        return m_PlacedGuideAnchors == 3;
    }

    public bool WidgetsArePlaced()
    {
        return m_WidgetsArePlaced;
    }

    public void Initialize()
    {
        ARGlassController.Instance.Initialize(InitCallback);
    }

    public void StartCoSLAM()
    {
        ARGlassController.Instance.StartCoSLAM(CoSLAMIpAddress, CoSLAMPort);
        m_InitDebugText.text = "Connect to CoSLAM server";
    }

    public void EnableFusion()
    {        
        ARGlassController.Instance.EnableCoSLAMFusion(true);
        m_InitDebugText.text = "Enable fusion";
    }

    void InitCallback(bool isFinished, string message)
    {
        m_InitDebugText.text = "Finished: " + isFinished + "\nMessage: " + message;

        if(isFinished)
        {
            if(ARGlassController.Instance.IsInitialized())
            {
                // Success
            }
            else
            {
                // Fail
            }
        }
    }

    private void SpawnWidgets(Vector3[] positions, Quaternion[] rotations)
    {
        float L_scale = 0.5f;
        float XL_scale = 0.8f;

        Vector3 sideX = positions[1] - positions[0];
        Vector3 sideZ = positions[2] - positions[0];
        Vector3 sideY = Vector3.Cross(sideZ, sideX).normalized * 2.5f;
        
        MusicPlayer = Instantiate(
            MusicPlayerPrefab,
            ((positions[1] + positions[0] + sideY) / 2),
            Quaternion.Euler(0.0f, 0.0f, 0.0f));
        MusicPlayer.transform.localScale = new Vector3(L_scale, L_scale, L_scale);
        MusicPlayer.GetComponent<IWidget>().Show();

        Gallery = Instantiate(
            GalleryPrefab,
            ((positions[1] + positions[2]) / 2),
            Quaternion.identity);
        Gallery.transform.Translate(positions[3] - positions[0]);
        Gallery.transform.Translate((positions[0] - positions[1]) / 5);
        Gallery.transform.rotation = Quaternion.Euler(-90.0f, 180.0f, 90.0f);
        Gallery.transform.localScale = new Vector3(XL_scale, XL_scale, XL_scale);
        Gallery.GetComponent<IWidget>().Show();

        Cloud = Instantiate(
            CloudPrefab,
            ((positions[3] + positions[1]) / 2),
            Quaternion.Euler(0.0f, 180.0f, 0.0f));
        Cloud.transform.Translate((positions[2] - positions[0]) / 3);
        Cloud.transform.localScale = new Vector3(XL_scale, XL_scale, XL_scale);
        Cloud.GetComponent<IWidget>().Show();

        _ShowAndroidToastMessage("Widgets Created");
        m_WidgetsArePlaced = true;
    }

    private void SetAimFromGazing()
    {
        Aim.transform.position = FirstPersonCamera.transform.position + FirstPersonCamera.transform.forward * 0.1f;
        Aim.transform.rotation = FirstPersonCamera.transform.rotation;
    }

    private void SpawnGuideAnchor(TrackableHit hit)
    {
        AnchorMarkers[m_PlacedGuideAnchors] = Instantiate(AnchorPrefab, hit.Pose.position, hit.Pose.rotation);
        AnchorMarkers[m_PlacedGuideAnchors].transform.localScale -= new Vector3(WidgetScale, WidgetScale, WidgetScale);
        AnchorMarkers[m_PlacedGuideAnchors].transform.Rotate(0, 180.0f, 0, Space.Self);

        GuideAnchors[m_PlacedGuideAnchors] = hit.Trackable.CreateAnchor(hit.Pose);
        AnchorMarkers[m_PlacedGuideAnchors].transform.parent = GuideAnchors[m_PlacedGuideAnchors].transform;
        m_LocalAnchorPoisitions[m_PlacedGuideAnchors] = GuideAnchors[m_PlacedGuideAnchors].transform.position;
        m_LocalAnchorRotations[m_PlacedGuideAnchors] = GuideAnchors[m_PlacedGuideAnchors].transform.rotation;

        _ShowAndroidToastMessage("Anchor(" + (m_PlacedGuideAnchors + 1) + "/3) Created");

        m_PlacedGuideAnchors++;
    }

    private void RecognizeGazing()
    {
        RaycastHit hit;
        if (Physics.Raycast(Aim.transform.position, Aim.transform.forward, out hit))
        {
            HitObject = hit.transform.gameObject;
            HitObject.GetComponent<IWidget>().HoverIn();
        }
        else if (HitObject != null)
        {
            HitObject.GetComponent<IWidget>().HoverOut();
            HitObject = null;
        }
    }
    private void _DestroyAnchorMarkers()
    {
        if (AnchorMarkers[0]) Destroy(AnchorMarkers[0]);
        if (AnchorMarkers[1]) Destroy(AnchorMarkers[1]);
        if (AnchorMarkers[2]) Destroy(AnchorMarkers[2]);
        if (AnchorMarkers[3]) Destroy(AnchorMarkers[3]);
        AnchorMarkers = null;
    }

    private void _ShowAndroidToastMessage(string message)
    {
        AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject unityActivity =
            unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

        if (unityActivity != null)
        {
            AndroidJavaClass toastClass = new AndroidJavaClass("android.widget.Toast");
            unityActivity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
            {
                AndroidJavaObject toastObject =
                    toastClass.CallStatic<AndroidJavaObject>(
                        "makeText", unityActivity, message, 0);
                toastObject.Call("show");
            }));
        }
    }
}
