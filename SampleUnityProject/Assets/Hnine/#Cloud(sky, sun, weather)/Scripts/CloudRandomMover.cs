﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace hnine.arglass {
    public class CloudRandomMover : MonoBehaviour {
        public List<Transform> CloudArr;
        [Range (0, 2)]
        public float[] CloudSpeedArr;
        [Range (-2.5f, 2.5f)]
        public float[] CloudDepthArr;
        public float minPosX, maxPosx;

        // Start is called before the first frame update
        void Start () {
            // CloudArr = new List<Transform> ();
            // for (int i = 0; i < CloudArr.Count; i++) {
            //     // CloudArr.Add (transform.GetChild (i));
            //     CloudDepthArr[i] = CloudArr[i].localPosition.z;
            // }
        }
        void Update () {
            if (CloudArr.Count > 0) {
                //for (int i = 0; i < CloudArr.Count; i++) {
                //  if (CloudArr[i].localPosition.x < maxPosx) {
                //    CloudArr[i].localPosition = new Vector3 (minPosX, CloudArr[i].localPosition.y, CloudArr[i].localPosition.z);
                // Debug.Log (CloudArr[i].name);
                //   }
                // }
                for (int i = 0; i < CloudArr.Count; i++) {
                    CloudArr[i].localPosition = new Vector3 (CloudArr[i].localPosition.x - (Time.deltaTime * CloudSpeedArr[i]), CloudArr[i].localPosition.y, CloudArr[i].localPosition.z);
                }
            }
        }
    }
}