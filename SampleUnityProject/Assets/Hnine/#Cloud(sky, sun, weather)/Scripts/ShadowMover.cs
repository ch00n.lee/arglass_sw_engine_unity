﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowMover : MonoBehaviour {
    private Transform transParent;
    // Start is called before the first frame update
    void Start () {
        transParent = transform.parent;
        GetComponent<SpriteRenderer>().enabled = true;
    }

    void Update () {
        transform.position = new Vector3 (transParent.position.x, 0, transParent.position.z);
    }
}