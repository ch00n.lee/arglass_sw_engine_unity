﻿using DG.Tweening;
using hnine.arglass;
using UnityEngine;

namespace hnine.arglass {
    public class CloudHandler : IWidget {
        private static readonly string TELEPORT = "_Teleport";
        private static readonly string TELEPORTLINE = "_LineNoiseThickness";
        private static readonly string ALPHA = "_Alpha";
        private static readonly string OPACITY = "_Opacity";
        public float _teleportTweenValue, _teleportLineThickness, _skydomeAlphaTweenValue, _sunGlowAlphaTweenValue;
        public Renderer[] rendererTextWireframe;
        public Renderer rendererSkyDome;
        public Renderer[] rendererSunGlow;
        public Renderer rendererSunCircle;
        public ParticleSystem particleGlow;
        public ParticleSystem[] goClouds;
        private Vector3[] scaleClouds;
        public Vector3[] positionNormalClouds, positionRaninClouds, positionSnowClouds;
        public GameObject goRain, goThunder;
        int count = 0;
        bool isShow;
        public Color cloudColor1, cloudColor2, cloudColor3;
        public Color skyColor1, skyColor2;
        public SpriteRenderer _skyColorTween, _sunColorTween;
        bool isSkyTween;
        public Transform transformSun;
        public GameObject goTextNormal, goTextRainy;
        void Start () {
            CloudInit ();
        }

        private void CloudInit () {
            goRain.SetActive (false);
            goThunder.SetActive (false);
            goTextNormal.SetActive (true);
            goTextRainy.SetActive (false);
            transformSun.localPosition = new Vector3 (transformSun.localPosition.x, 3, transformSun.localPosition.z);
            _skyColorTween.color = skyColor1;
            _sunColorTween.color = new Vector4 (1, 1, 1, 0);
            particleGlow.emissionRate = 0;
            _teleportTweenValue = 0.6f; // 0.1f
            _teleportLineThickness = 0.5f;
            _skydomeAlphaTweenValue = 0;
            _sunGlowAlphaTweenValue = 0;
            scaleClouds = new Vector3[goClouds.Length];
            count = 0;
            foreach (var item in goClouds) {
                item.gameObject.SetActive (false);
                scaleClouds[count] = item.gameObject.transform.localScale;
                item.gameObject.transform.localPosition = positionNormalClouds[count];
                count++;

                item.gameObject.transform.localScale = Vector3.zero;
            }
        }
        
        private void WeatherNormal () {
            goThunder.SetActive (false);
            goRain.SetActive (false);
            count = 0;
            isSkyTween = true;
            foreach (var item in goClouds) {
                item.playbackSpeed = 9;
                item.startColor = cloudColor1;
                item.gameObject.transform.DOLocalMove (positionNormalClouds[count], 2f).SetEase (Ease.InOutQuart);
                count++;
            }
            _skyColorTween.DOColor (skyColor1, 1);

            if (isShow) {
                DOTween.To (() => _teleportTweenValue, temp => _teleportTweenValue = temp, 0.6f, 1.3f).OnComplete (() => {
                    goTextNormal.SetActive (true);
                    goTextRainy.SetActive (false);
                    DOTween.To (() => _teleportTweenValue, temp => _teleportTweenValue = temp, -0.1f, 1.5f).OnComplete (() => { });
                });
            }
            Invoke ("CloudSpeedReset", 1);
        }
        private void WeatherRaniny () {
            goThunder.SetActive (true);
            goRain.SetActive (true);
            count = 0;
            isSkyTween = true;
            foreach (var item in goClouds) {
                item.playbackSpeed = 9;
                item.startColor = cloudColor2;
                item.gameObject.transform.DOLocalMove (positionRaninClouds[count], 2f).SetEase (Ease.InOutQuart);
                count++;
            }
            _skyColorTween.DOColor (skyColor2, 1);

            DOTween.To (() => _teleportTweenValue, temp => _teleportTweenValue = temp, 0.6f, 1.3f).OnComplete (() => {
                goTextNormal.SetActive (false);
                goTextRainy.SetActive (true);
                DOTween.To (() => _teleportTweenValue, temp => _teleportTweenValue = temp, -0.1f, 1.3f).OnComplete (() => { });
            });

            Invoke ("CloudSpeedReset", 1);
        }

        void CloudSpeedReset () {
            foreach (var item in goClouds) {
                item.playbackSpeed = 0.0f;
            }
            isSkyTween = false;
        }

        public override void Show () {
            goTextNormal.SetActive (true);
            goTextRainy.SetActive (false);
            WeatherNormal ();            
            isShow = true;
            count = 0;
            foreach (var item in goClouds) {
                item.gameObject.SetActive (true);
                item.gameObject.transform.DOScale (scaleClouds[count], 1f).SetEase (Ease.OutQuart);
                count++;
            }
            particleGlow.emissionRate = 60;
            DOTween.To (() => _teleportLineThickness, temp => _teleportLineThickness = temp, 0.2f, 3f);
            DOTween.To (() => _teleportTweenValue, temp => _teleportTweenValue = temp, -0.1f, 3f).OnComplete (() => {
                particleGlow.emissionRate = 0;
                foreach (var item in goClouds) {
                    item.playbackSpeed = 0.2f;
                }
            });
            DOTween.To (() => _skydomeAlphaTweenValue, temp => _skydomeAlphaTweenValue = temp, 1f, 2f);
            DOTween.To (() => _sunGlowAlphaTweenValue, temp => _sunGlowAlphaTweenValue = temp, 1f, 2f);
            _sunColorTween.DOColor (new Vector4 (1, 206f / 255f, 141f / 255f, 1), 1);
            transformSun.DOLocalMoveY (4, 2f).SetEase (Ease.OutQuart);
            Invoke ("CloudSpeedReset", 1);
        }
        public override void Hide () {
            count = 0;
            foreach (var item in goClouds) {
                int idx = count;
                item.gameObject.transform.DOScale (0.0001f, 1.3f).SetEase (Ease.OutQuart).SetDelay (idx * 0.05f).OnComplete (() => {
                    goClouds[idx].gameObject.SetActive (false);
                });
                count++;
            }
            particleGlow.emissionRate = 60;
            DOTween.To (() => _teleportLineThickness, temp => _teleportLineThickness = temp, 0.5f, 3f).OnComplete (() => {
                particleGlow.emissionRate = 0;

                isShow = false;
            });
            DOTween.To (() => _teleportTweenValue, temp => _teleportTweenValue = temp, 0.6f, 3f).OnComplete (() => { });
            DOTween.To (() => _skydomeAlphaTweenValue, temp => _skydomeAlphaTweenValue = temp, 0f, 2f);
            DOTween.To (() => _sunGlowAlphaTweenValue, temp => _sunGlowAlphaTweenValue = temp, 0f, 2f);
            _sunColorTween.DOColor (new Vector4 (1, 206f / 255f, 141f / 255f, 0), 1);
            transformSun.DOLocalMoveY (3, 2f).SetEase (Ease.InOutQuart);

            goRain.SetActive (false);
            goThunder.SetActive (false);
        }
        public override void HoverIn () {
            WeatherRaniny ();
        }
        public override void HoverOut () {
            WeatherNormal ();
        }
        private void Update () {
            foreach (Renderer v in rendererTextWireframe) {
                v.material.SetFloat (TELEPORT, _teleportTweenValue);
                v.material.SetFloat (TELEPORTLINE, _teleportLineThickness);
            }
            rendererSunCircle.material.SetColor ("_Color", _sunColorTween.color);
            //Sun Glow
            foreach (var item in rendererSunGlow)
                item.material.SetFloat (OPACITY, _sunGlowAlphaTweenValue);

            //SkyDome
            rendererSkyDome.material.SetFloat (ALPHA, _skydomeAlphaTweenValue);
            if (!isSkyTween) return;
            rendererSkyDome.material.SetColor ("_Color", _skyColorTween.color);
        }
    }
}