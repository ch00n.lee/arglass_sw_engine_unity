﻿using UnityEngine;

namespace hnine.arglass {
    public class LootAtMainCamera : MonoBehaviour {
        public Transform target;

        private void Awake () {
            if (Camera.main != null)
                target = Camera.main.transform;
        }

        private void Update () {
            transform.LookAt (target);
        }
    }
}