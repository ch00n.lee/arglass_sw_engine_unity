﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace hnine.arglass {
    public class ColorBlinker : MonoBehaviour {
        public SpriteRenderer sprite;
        public AnimationCurve intensityCurve = AnimationCurve.EaseInOut (0f, 1f, 1f, 0f);
        public float peakIntensity = 0.7f;
        public float duration = 1.0f;
        public float playbackTime;

        void Update () {
            playbackTime = Time.deltaTime * UnityEngine.Random.Range (1f, 3f);
            float d = Mathf.Clamp01 ((playbackTime % duration) / (duration + float.Epsilon));
            float targetIntensity = Mathf.Lerp (0f, peakIntensity, intensityCurve.Evaluate (d));
            sprite.color = new Vector4 (sprite.color.r, sprite.color.g, sprite.color.b, targetIntensity);
        }
    }
}