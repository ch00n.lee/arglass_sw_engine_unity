﻿using UnityEngine;

namespace hnine.arglass {
    public class IWidget : MonoBehaviour {
        public virtual void Show () { }
        public virtual void HoverIn () { }
        public virtual void HoverOut () { }
        public virtual void Hide () { }
    }
}