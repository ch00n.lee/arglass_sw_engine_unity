﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace hnine.arglass {
    public class GalleryShadowHandler : MonoBehaviour {
        public RectTransform rectTarget;
        public float posY, posZ;
        public float width;
        public float height;
        [Range (0, 1)]
        public float alpha;
        RectTransform myRect;
        Vector3 posShadow;
        Image myImage;
        // Start is called before the first frame update
        void Start () {
            myRect = GetComponent<RectTransform> ();
        }

        // Update is called once per frame
        void Update () {
            // if (!myRect){
            myRect.SetSizeWithCurrentAnchors (RectTransform.Axis.Horizontal, rectTarget.rect.width + width);
            myRect.SetSizeWithCurrentAnchors (RectTransform.Axis.Vertical, rectTarget.rect.height + height);
            myImage = GetComponent<Image> ();
            posShadow = new Vector3 (0, posY, posZ);
            myRect.localPosition = rectTarget.localPosition + posShadow;
            if (rectTarget.rect.width < 100) {
                myImage.color = new Vector4 (myImage.color.r, myImage.color.g, myImage.color.b, alpha * rectTarget.rect.width / 100);
            } else {
                myImage.color = new Vector4 (myImage.color.r, myImage.color.g, myImage.color.b, alpha);
            }
            // }
        }
    }
}