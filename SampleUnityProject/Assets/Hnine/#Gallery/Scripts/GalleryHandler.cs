﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

namespace hnine.arglass {
    public class GalleryHandler : IWidget {
        public Animator animator;

        public MeshRenderer[] rendererFrames;
        public RawImage[] imagePictures;
        public GameObject[] goFrames;
        public GalleryShadowHandler[] shadowHandlers;
        public ParticleSystem particleGlow;
        public Light lightAmbient1, lightAmbient2;
        public VideoPlayer videoNightSky, videoSkyLoop, videoWoman, videoWave;
        public RawImage imageWave, imageGirls;
        private bool _isHover, _isShow;
        private float _shadowAlpha = 0, _lightValue;
        private float _originPosY;
        private float _videoTweenTime = 0, _videoTweenTimeWoman, _videoTweenTimeWave;
        private bool _isRewind;
        private static readonly int TweenValue = Animator.StringToHash ("TweenValue");
        private static readonly int InCurve = Animator.StringToHash ("InCurve");
        private static readonly int OutCurve = Animator.StringToHash ("OutCurve");
        private static readonly int In = Animator.StringToHash ("HoverIn");
        private static readonly string TELEPORT = "_Teleport";
        private static readonly string TELEPORTLINE = "_LineNoiseThickness";
        public float inCurveValue, outCurveValue;
        public float _teleportTweenValue, _teleportLineThickness;
        bool _isTeleport;

        public Renderer[] rendererWireframe;
        public Vector3[] posStartWire;

        private void Start () {
            _originPosY = transform.localPosition.y;
            GalleryInit ();
        }

        private void GalleryInit () {

            particleGlow.emissionRate = 0;
            _teleportTweenValue = -0.75f;
            _teleportLineThickness = 0.1f;
            _lightValue = 0;
            foreach (var t in rendererFrames)
                t.material.DOFade (0, 0f);

            foreach (var t in imagePictures)
                t.DOFade (0, 0f);

            foreach (var t in shadowHandlers)
                t.alpha = 0;

            foreach (var t in goFrames)
                t.SetActive (false);
        }

        public override void Show () {
            _isShow = true;
            _isHover = false;
            _isTeleport = true;
            particleGlow.emissionRate = 60;

            DOTween.To (() => _teleportLineThickness, temp => _teleportLineThickness = temp, 0.25f, 1.5f);
            DOTween.To (() => _teleportTweenValue, temp => _teleportTweenValue = temp, -0.54f, 2f).OnComplete (() => {
                particleGlow.emissionRate = 0;
                DOTween.To (() => _lightValue, temp => _lightValue = temp, 2, 1).OnComplete (() => { });
                DOTween.To (() => _teleportTweenValue, temp => _teleportTweenValue = temp, -0.75f, 2.5f).OnComplete (() => {
                    _isTeleport = false;
                });
                DOTween.To (() => _teleportLineThickness, temp => _teleportLineThickness = temp, 0.1f, 1.5f);
                transform.DOLocalMoveY (_originPosY, 1.5f).SetEase (Ease.OutQuart);

                foreach (var t in goFrames)
                    t.SetActive (true);

                foreach (var t in rendererFrames)
                    t.material.DOFade (1, 1f);

                foreach (var t in imagePictures)
                    t.DOFade (1, 1f);

                DOTween.To (() => _shadowAlpha, temp => _shadowAlpha = temp, 0.5f, 1);
                DOTween.To (() => _lightValue, temp => _lightValue = temp, 0, 1);
            });
            videoNightSky.Play ();
            videoNightSky.playbackSpeed = 0;
            videoSkyLoop.Play ();
            videoSkyLoop.playbackSpeed = 0;
            videoWave.Play ();
            videoWave.playbackSpeed = 0;
            videoWoman.Play ();
            videoWoman.playbackSpeed = 0;
        }

        public override void Hide () {
            _isHover = false;
            particleGlow.emissionRate = 60;
            _isTeleport = true;
            DOTween.To (() => _teleportTweenValue, temp => _teleportTweenValue = temp, -0.54f, 1f);
            DOTween.To (() => _teleportLineThickness, temp => _teleportLineThickness = temp, 0.25f, 1.5f);
            DOTween.To (() => _lightValue, temp => _lightValue = temp, 2, 1).OnComplete (() => {
                particleGlow.emissionRate = 0;

                transform.DOLocalMoveY (_originPosY, 1.5f).SetEase (Ease.OutQuart)
                    .OnComplete (() => {
                        _isShow = false;
                    });
                DOTween.To (() => _teleportLineThickness, temp => _teleportLineThickness = temp, 0.1f, 1.5f).SetDelay (0.5f);
                DOTween.To (() => _teleportTweenValue, temp => _teleportTweenValue = temp, -0.75f, 1.5f).SetDelay (0.5f).OnComplete (() => {
                    _isTeleport = false;
                });

                DOTween.To (() => _lightValue, temp => _lightValue = temp, 0, 1);
                foreach (var t in rendererFrames)
                    t.material.DOFade (0, 1f);

                foreach (var t in imagePictures)
                    t.DOFade (0, 1f);

                DOTween.To (() => _shadowAlpha, temp => _shadowAlpha = temp, 0f, 1).OnComplete (() => {
                    foreach (var t in goFrames)
                        t.SetActive (false);
                });
            });

            videoNightSky.Stop ();
            videoSkyLoop.Stop ();
            videoWave.Stop ();
            videoWoman.Stop ();
        }

        public override void HoverIn () {
            _isHover = true;
            animator.SetBool (In, true);
            _isRewind = false;
            imageWave.DOKill ();
            imageGirls.DOKill ();
            imageWave.DOFade (0, 1f).SetEase (Ease.Linear);
            imageGirls.DOFade (0, 1f).SetEase (Ease.Linear);
            videoNightSky.Play ();
            videoNightSky.playbackSpeed = 1;
            videoSkyLoop.Play ();
            videoSkyLoop.playbackSpeed = 1;
            videoWave.Play ();
            videoWave.playbackSpeed = 1;
            videoWoman.Play ();
            videoWoman.playbackSpeed = 1;
        }

        public override void HoverOut () {
            _isHover = false;
            _isRewind = true;
            videoWave.playbackSpeed = 0;
            videoWoman.playbackSpeed = 0;
            videoNightSky.playbackSpeed = 0;
            animator.SetBool (In, false);
            videoNightSky.playbackSpeed = 0;
            _videoTweenTime = (float) videoNightSky.time;
            _videoTweenTimeWave = (float) videoWave.time;
            _videoTweenTimeWoman = (float) videoWoman.time;
            DOTween.To (() => _videoTweenTime, temp => _videoTweenTime = temp, 0f, 2f).SetEase (Ease.OutCubic);
            DOTween.To (() => _videoTweenTimeWave, temp => _videoTweenTimeWave = temp, 0f, 2f).SetEase (Ease.OutCubic);
            DOTween.To (() => _videoTweenTimeWoman, temp => _videoTweenTimeWoman = temp, 0f, 2f).SetEase (Ease.OutCubic).OnComplete (() => {
                _isRewind = false;
                // videoWave.playbackSpeed = 0;
                // videoWoman.playbackSpeed = 0;
                // videoNightSky.playbackSpeed = 0;
                videoWave.Stop ();
                videoWoman.Stop ();
                videoNightSky.Stop();
            });
            imageWave.DOKill ();
            imageGirls.DOKill ();
            imageWave.DOFade (1, 1f).SetEase (Ease.Linear);
            imageGirls.DOFade (1, 1f).SetEase (Ease.Linear);
            videoSkyLoop.Stop ();
        }

        private void Update () {
            lightAmbient1.intensity = _lightValue;
            lightAmbient2.intensity = _lightValue;

            if (_isTeleport) {
                foreach (Renderer v in rendererWireframe) {
                    v.material.SetFloat (TELEPORT, _teleportTweenValue);
                    v.material.SetFloat (TELEPORTLINE, _teleportLineThickness);
                }
            }

            if (_isRewind) {
                videoNightSky.time = _videoTweenTime;
                videoWoman.time = _videoTweenTimeWoman;
                videoWave.time = _videoTweenTimeWave;
            }
            if (!_isShow) return;

            animator.SetBool (In, _isHover);

            inCurveValue = animator.GetFloat (InCurve);
            outCurveValue = animator.GetFloat (OutCurve);

            animator.SetFloat (TweenValue, _isHover ? inCurveValue : outCurveValue);
            foreach (var t in shadowHandlers)
                t.alpha = _shadowAlpha;

        }
    }
}