// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Hnine/Teleport/NeflixFrame"
{
	Properties
	{
		[Toggle]_ChangeEffect("Change Effect", Float) = 1
		_Teleport("Teleport", Range( -2 , 2)) = 0.1753444
		[Toggle]_Reverse("Reverse", Float) = 0
		_mail_main_DefaultMaterial_Metallic("mail_main_DefaultMaterial_Metallic", 2D) = "white" {}
		_NegativeNumber("Negative Number", Float) = -10
		_mail_main_DefaultMaterial_Roughness("mail_main_DefaultMaterial_Roughness", 2D) = "white" {}
		_PositiveNumber("Positive Number", Float) = 10
		_NormalNoiseSpeed("Normal Noise Speed", Float) = 1
		_Cutoff( "Mask Clip Value", Float ) = 0.01
		_NormalNoiseTiling("Normal Noise Tiling", Vector) = (25,25,0,0)
		_NormalNoiseBooster("Normal Noise Booster", Float) = 1
		_LineNoiseSpeed("Line Noise Speed", Float) = 1
		_LineNoiseScale("Line Noise Scale", Float) = 5
		_LineNoiseThickness("Line Noise Thickness", Float) = 0.63
		[HDR]_GlowColor("Glow Color", Color) = (0.7243609,3.380351,3.843137,0)
		_Tint("Tint", Color) = (0,0,0,0)
		_AmbientOcclusion("Ambient Occlusion", 2D) = "white" {}
		_Albedo("Albedo", 2D) = "white" {}
		_NormalMap("NormalMap", 2D) = "bump" {}
		_OffsetStrength("Offset Strength", Float) = 0.28
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "TransparentCutout"  "Queue" = "AlphaTest+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows vertex:vertexDataFunc 
		struct Input
		{
			float2 uv_texcoord;
			float3 worldPos;
		};

		uniform float _Teleport;
		uniform float _Reverse;
		uniform float _NegativeNumber;
		uniform float _PositiveNumber;
		uniform float _OffsetStrength;
		uniform float _ChangeEffect;
		uniform float2 _NormalNoiseTiling;
		uniform float _NormalNoiseSpeed;
		uniform float _NormalNoiseBooster;
		uniform float _LineNoiseScale;
		uniform float _LineNoiseSpeed;
		uniform float _LineNoiseThickness;
		uniform sampler2D _NormalMap;
		uniform float4 _NormalMap_ST;
		uniform sampler2D _Albedo;
		uniform float4 _Albedo_ST;
		uniform sampler2D _AmbientOcclusion;
		uniform float4 _AmbientOcclusion_ST;
		uniform float4 _Tint;
		uniform float4 _GlowColor;
		uniform sampler2D _mail_main_DefaultMaterial_Metallic;
		uniform float4 _mail_main_DefaultMaterial_Metallic_ST;
		uniform sampler2D _mail_main_DefaultMaterial_Roughness;
		uniform float4 _mail_main_DefaultMaterial_Roughness_ST;
		uniform float _Cutoff = 0.01;


		float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }

		float snoise( float2 v )
		{
			const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
			float2 i = floor( v + dot( v, C.yy ) );
			float2 x0 = v - i + dot( i, C.xx );
			float2 i1;
			i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
			float4 x12 = x0.xyxy + C.xxzz;
			x12.xy -= i1;
			i = mod2D289( i );
			float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
			float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
			m = m * m;
			m = m * m;
			float3 x = 2.0 * frac( p * C.www ) - 1.0;
			float3 h = abs( x ) - 0.5;
			float3 ox = floor( x + 0.5 );
			float3 a0 = x - ox;
			m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
			float3 g;
			g.x = a0.x * x0.x + h.x * x0.y;
			g.yz = a0.yz * x12.xz + h.yz * x12.yw;
			return 130.0 * dot( m, g );
		}


		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float3 ase_vertex3Pos = v.vertex.xyz;
			float4 transform19 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Y_Gradient17 = saturate( ( ( ( transform19.x / 14.0 ) + _Teleport ) / lerp(_NegativeNumber,_PositiveNumber,_Reverse) ) );
			float mulTime6 = _Time.y * _NormalNoiseSpeed;
			float2 panner5 = ( mulTime6 * float2( 0,-1 ) + float2( 0,0 ));
			float2 uv_TexCoord2 = v.texcoord.xy * _NormalNoiseTiling + panner5;
			float simplePerlin2D1 = snoise( uv_TexCoord2 );
			float Noise110 = ( simplePerlin2D1 + _NormalNoiseBooster );
			float2 temp_cast_1 = (_LineNoiseScale).xx;
			float mulTime73 = _Time.y * _LineNoiseSpeed;
			float2 panner72 = ( mulTime73 * float2( -1,-1 ) + float2( 0,0 ));
			float2 uv_TexCoord66 = v.texcoord.xy * temp_cast_1 + panner72;
			float Noise269 = step( frac( uv_TexCoord66.x ) , _LineNoiseThickness );
			float Noise76 = lerp(Noise110,Noise269,_ChangeEffect);
			float3 VertexOffset57 = ( ( ( ase_vertex3Pos * Y_Gradient17 ) * _OffsetStrength ) * Noise76 );
			v.vertex.xyz += VertexOffset57;
		}

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_NormalMap = i.uv_texcoord * _NormalMap_ST.xy + _NormalMap_ST.zw;
			float3 NormalMap49 = UnpackNormal( tex2D( _NormalMap, uv_NormalMap ) );
			o.Normal = NormalMap49;
			float2 uv_Albedo = i.uv_texcoord * _Albedo_ST.xy + _Albedo_ST.zw;
			float2 uv_AmbientOcclusion = i.uv_texcoord * _AmbientOcclusion_ST.xy + _AmbientOcclusion_ST.zw;
			float4 Albedo46 = ( tex2D( _Albedo, uv_Albedo ) * tex2D( _AmbientOcclusion, uv_AmbientOcclusion ) * _Tint );
			float4 temp_cast_0 = (1.5).xxxx;
			o.Albedo = pow( Albedo46 , temp_cast_0 ).rgb;
			float mulTime6 = _Time.y * _NormalNoiseSpeed;
			float2 panner5 = ( mulTime6 * float2( 0,-1 ) + float2( 0,0 ));
			float2 uv_TexCoord2 = i.uv_texcoord * _NormalNoiseTiling + panner5;
			float simplePerlin2D1 = snoise( uv_TexCoord2 );
			float Noise110 = ( simplePerlin2D1 + _NormalNoiseBooster );
			float2 temp_cast_2 = (_LineNoiseScale).xx;
			float mulTime73 = _Time.y * _LineNoiseSpeed;
			float2 panner72 = ( mulTime73 * float2( -1,-1 ) + float2( 0,0 ));
			float2 uv_TexCoord66 = i.uv_texcoord * temp_cast_2 + panner72;
			float Noise269 = step( frac( uv_TexCoord66.x ) , _LineNoiseThickness );
			float Noise76 = lerp(Noise110,Noise269,_ChangeEffect);
			float3 ase_vertex3Pos = mul( unity_WorldToObject, float4( i.worldPos , 1 ) );
			float4 transform19 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Y_Gradient17 = saturate( ( ( ( transform19.x / 14.0 ) + _Teleport ) / lerp(_NegativeNumber,_PositiveNumber,_Reverse) ) );
			float4 Emission38 = ( _GlowColor * ( Noise76 * Y_Gradient17 ) );
			o.Emission = Emission38.rgb;
			float2 uv_mail_main_DefaultMaterial_Metallic = i.uv_texcoord * _mail_main_DefaultMaterial_Metallic_ST.xy + _mail_main_DefaultMaterial_Metallic_ST.zw;
			o.Metallic = tex2D( _mail_main_DefaultMaterial_Metallic, uv_mail_main_DefaultMaterial_Metallic ).r;
			float2 uv_mail_main_DefaultMaterial_Roughness = i.uv_texcoord * _mail_main_DefaultMaterial_Roughness_ST.xy + _mail_main_DefaultMaterial_Roughness_ST.zw;
			o.Smoothness = ( 1.0 - tex2D( _mail_main_DefaultMaterial_Roughness, uv_mail_main_DefaultMaterial_Roughness ) ).r;
			o.Alpha = 1;
			float temp_output_30_0 = ( Y_Gradient17 * 1.0 );
			float Opacity_Mask27 = ( ( ( ( 1.0 - Y_Gradient17 ) * Noise76 ) - temp_output_30_0 ) + ( 1.0 - temp_output_30_0 ) );
			clip( Opacity_Mask27 - _Cutoff );
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=16900
-2560;23;2560;1395;4112.186;1007.897;1.141674;True;True
Node;AmplifyShaderEditor.CommentaryNode;77;-5583.108,-719.9613;Float;False;1577.897;543.2195;Comment;9;69;71;70;74;73;68;72;66;67;Noise Type2;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;12;-5595.233,-1230.026;Float;False;1636.574;409.8639;Comment;9;10;8;9;1;2;5;4;6;7;Noise Type1;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;7;-5545.232,-953.8776;Float;False;Property;_NormalNoiseSpeed;Normal Noise Speed;7;0;Create;True;0;0;False;0;1;1.5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;74;-5533.108,-435.8932;Float;False;Property;_LineNoiseSpeed;Line Noise Speed;11;0;Create;True;0;0;False;0;1;2.9;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;23;-3112.442,-584.9584;Float;False;1572.534;604.4539;Comment;10;17;22;20;15;64;21;63;16;19;13;Y_Gradient;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleTimeNode;6;-5378.947,-947.2261;Float;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;73;-5366.823,-429.2416;Float;True;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.PosVertexDataNode;13;-3062.442,-534.9582;Float;True;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ObjectToWorldTransfNode;19;-2801.625,-457.0649;Float;False;1;0;FLOAT4;0,0,0,1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;68;-5191.854,-670.9613;Float;False;Property;_LineNoiseScale;Line Noise Scale;12;0;Create;True;0;0;False;0;5;20;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;5;-5159.45,-991.1259;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,-1;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;72;-5185.627,-485.5721;Float;True;3;0;FLOAT2;0,0;False;2;FLOAT2;-1,-1;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.Vector2Node;4;-5136.835,-1145.439;Float;False;Property;_NormalNoiseTiling;Normal Noise Tiling;9;0;Create;True;0;0;False;0;25,25;25,25;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.RangedFloatNode;63;-2742.804,-90.2652;Float;False;Property;_PositiveNumber;Positive Number;6;0;Create;True;0;0;False;0;10;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;2;-4958.578,-1153.421;Float;True;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleDivideOpNode;86;-2631.296,-730.9802;Float;True;2;0;FLOAT;0;False;1;FLOAT;14;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;16;-2876.044,-265.8459;Float;False;Property;_Teleport;Teleport;1;0;Create;True;0;0;False;0;0.1753444;-0.69;-2;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;66;-4937.401,-668.7603;Float;True;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;21;-2746.643,-170.7305;Float;False;Property;_NegativeNumber;Negative Number;4;0;Create;True;0;0;False;0;-10;-1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;1;-4679.216,-1180.027;Float;True;Simplex2D;1;0;FLOAT2;0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;9;-4632.065,-944.9039;Float;False;Property;_NormalNoiseBooster;Normal Noise Booster;10;0;Create;True;0;0;False;0;1;0.28;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;15;-2491.707,-467.5602;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;71;-4685.671,-418.3716;Float;False;Property;_LineNoiseThickness;Line Noise Thickness;13;0;Create;True;0;0;False;0;0.63;0.1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.FractNode;67;-4675.467,-663.9541;Float;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ToggleSwitchNode;64;-2484.804,-171.2652;Float;False;Property;_Reverse;Reverse;2;0;Create;True;0;0;False;0;0;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;8;-4422.174,-1072.663;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;70;-4465.046,-666.5371;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;20;-2241.871,-439.349;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;69;-4238.71,-665.1257;Float;True;Noise2;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;22;-1994.938,-442.3246;Float;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;10;-4199.276,-1077.875;Float;True;Noise1;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;17;-1786.084,-446.4785;Float;True;Y_Gradient;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;34;-3084.171,248.0919;Float;False;1536.703;744.6415;Comment;9;27;33;32;31;25;30;29;24;18;Opacity Mask;1,1,1,1;0;0
Node;AmplifyShaderEditor.ToggleSwitchNode;75;-3863.961,-742.5559;Float;False;Property;_ChangeEffect;Change Effect;0;0;Create;True;0;0;False;0;1;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;76;-3607.987,-760.2094;Float;True;Noise;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;65;-3069.607,1187.872;Float;False;1491.307;595.9287;Comment;7;55;54;60;56;59;61;57;Vert Offset;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;18;-3019.594,298.0916;Float;True;17;Y_Gradient;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;24;-2768.695,301.8328;Float;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;53;-1544.235,-2005.125;Float;False;1529.429;699.265;Comment;7;45;42;43;44;46;48;49;Base Stuff;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;55;-3003.586,1481.244;Float;True;17;Y_Gradient;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;41;-3076.007,-1947.379;Float;False;1172.824;623.3817;Comment;5;36;39;37;40;38;Emission;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;29;-3034.172,755.3946;Float;True;17;Y_Gradient;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;26;-3622.076,171.342;Float;True;76;Noise;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PosVertexDataNode;54;-3019.607,1237.872;Float;True;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;43;-1494.235,-1735.869;Float;True;Property;_AmbientOcclusion;Ambient Occlusion;16;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;30;-2798.191,757.8898;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;60;-2695.367,1526.301;Float;True;Property;_OffsetStrength;Offset Strength;19;0;Create;True;0;0;False;0;0.28;0.05;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;36;-3017.007,-1553.996;Float;True;17;Y_Gradient;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;45;-1494.07,-1955.125;Float;True;Property;_Albedo;Albedo;17;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;56;-2698.949,1258.41;Float;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;25;-2516.716,303.5113;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;42;-1407.019,-1510.36;Float;False;Property;_Tint;Tint;15;0;Create;True;0;0;False;0;0,0,0,0;0.3490566,0.3490566,0.3490566,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;44;-1143.467,-1810.417;Float;True;3;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;31;-2239.766,451.6384;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;32;-2245.807,719.335;Float;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;59;-2387.157,1266.665;Float;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ColorNode;39;-2718.344,-1897.379;Float;False;Property;_GlowColor;Glow Color;14;1;[HDR];Create;True;0;0;False;0;0.7243609,3.380351,3.843137,0;0.1810902,0.632408,0.9607843,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;37;-2733.007,-1660.996;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;61;-2107.154,1258.92;Float;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;40;-2434.856,-1752.336;Float;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;46;-867.6167,-1807.508;Float;False;Albedo;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;48;-638.4065,-1838.336;Float;True;Property;_NormalMap;NormalMap;18;0;Create;True;0;0;False;0;None;None;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;33;-2004.412,595.8491;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;27;-1776.794,590.1847;Float;True;Opacity_Mask;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;47;197.087,-969.6018;Float;True;46;Albedo;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;85;322.9752,-719.4022;Float;False;Constant;_AlbedoPower;AlbedoPower;20;0;Create;True;0;0;False;0;1.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;81;-631.2903,-64.80286;Float;True;Property;_mail_main_DefaultMaterial_Roughness;mail_main_DefaultMaterial_Roughness;5;0;Create;True;0;0;False;0;22f260a4fbbc6ca4a86be79d5781f58d;22f260a4fbbc6ca4a86be79d5781f58d;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;57;-1811.8,1259.429;Float;True;VertexOffset;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;38;-2136.683,-1696.802;Float;True;Emission;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;49;-248.3064,-1845.395;Float;True;NormalMap;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;50;103.924,-625.2388;Float;True;49;NormalMap;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;28;412.0709,-44.14722;Float;True;27;Opacity_Mask;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;84;543.2901,-902.241;Float;False;2;0;COLOR;0,0,0,0;False;1;FLOAT;1.5;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;83;-346.325,-351.8237;Float;True;Property;_mail_main_DefaultMaterial_Metallic;mail_main_DefaultMaterial_Metallic;3;0;Create;True;0;0;False;0;87b3708f1738bd64c9fa48c58d8871a6;87b3708f1738bd64c9fa48c58d8871a6;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;11;325.7656,-531.2477;Float;True;38;Emission;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.OneMinusNode;82;-239.3596,-106.8445;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;58;411.1072,152.074;Float;True;57;VertexOffset;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;738.839,-599.7689;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;Hnine/Teleport/NeflixFrame;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;True;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Masked;0.01;True;True;0;False;TransparentCutout;;AlphaTest;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;8;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;6;0;7;0
WireConnection;73;0;74;0
WireConnection;19;0;13;0
WireConnection;5;1;6;0
WireConnection;72;1;73;0
WireConnection;2;0;4;0
WireConnection;2;1;5;0
WireConnection;86;0;19;1
WireConnection;66;0;68;0
WireConnection;66;1;72;0
WireConnection;1;0;2;0
WireConnection;15;0;86;0
WireConnection;15;1;16;0
WireConnection;67;0;66;1
WireConnection;64;0;21;0
WireConnection;64;1;63;0
WireConnection;8;0;1;0
WireConnection;8;1;9;0
WireConnection;70;0;67;0
WireConnection;70;1;71;0
WireConnection;20;0;15;0
WireConnection;20;1;64;0
WireConnection;69;0;70;0
WireConnection;22;0;20;0
WireConnection;10;0;8;0
WireConnection;17;0;22;0
WireConnection;75;0;10;0
WireConnection;75;1;69;0
WireConnection;76;0;75;0
WireConnection;24;0;18;0
WireConnection;30;0;29;0
WireConnection;56;0;54;0
WireConnection;56;1;55;0
WireConnection;25;0;24;0
WireConnection;25;1;26;0
WireConnection;44;0;45;0
WireConnection;44;1;43;0
WireConnection;44;2;42;0
WireConnection;31;0;25;0
WireConnection;31;1;30;0
WireConnection;32;0;30;0
WireConnection;59;0;56;0
WireConnection;59;1;60;0
WireConnection;37;0;26;0
WireConnection;37;1;36;0
WireConnection;61;0;59;0
WireConnection;61;1;26;0
WireConnection;40;0;39;0
WireConnection;40;1;37;0
WireConnection;46;0;44;0
WireConnection;33;0;31;0
WireConnection;33;1;32;0
WireConnection;27;0;33;0
WireConnection;57;0;61;0
WireConnection;38;0;40;0
WireConnection;49;0;48;0
WireConnection;84;0;47;0
WireConnection;84;1;85;0
WireConnection;82;0;81;0
WireConnection;0;0;84;0
WireConnection;0;1;50;0
WireConnection;0;2;11;0
WireConnection;0;3;83;0
WireConnection;0;4;82;0
WireConnection;0;10;28;0
WireConnection;0;11;58;0
ASEEND*/
//CHKSM=99C63798D0F07172808183A4A58CD192DD5C6956