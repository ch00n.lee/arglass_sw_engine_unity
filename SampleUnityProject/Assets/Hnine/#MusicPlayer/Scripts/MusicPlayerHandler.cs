﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace hnine.arglass {
    public class MusicPlayerHandler : IWidget {
        public Transform sqCircle;
        public ParticleSystem particleAura;
        public ParticleSystem particleGlow;
        public Light pointLight;

        public SpriteRenderer[] sprites;

        public Transform particleDonut;
        public AudioSource audioSource;
        private float _volume;

        public RectTransform rectImage;
        public Image textTitle;
        public RectTransform rectCoverHolder;
        public Vector3[] posShow, scaleShow;

        private bool isShow;
        private bool isHover;
        private float _tweenTime = 0.6f;

        private void Start () {
            Init ();

            // Show();
        }

        private void Init () {
            rectImage.GetComponent<Image> ().DOFade (0, 0f);
            textTitle.DOFade (0, 0f);
            rectImage.localPosition = new Vector3 (0, -230f, 0);
            textTitle.GetComponent<RectTransform> ().localPosition = new Vector3 (0, -458, 0);
            audioSource.Stop ();
            audioSource.volume = _volume;
            particleDonut.localScale = Vector3.zero;
            pointLight.range = 0;
            particleGlow.maxParticles = 0;
            sprites = sqCircle.GetComponentsInChildren<SpriteRenderer> ();
            posShow = new Vector3[sprites.Length];
            scaleShow = new Vector3[sprites.Length];
            if (sprites.Length <= 0) return;
            for (var i = 0; i < sprites.Length; i++) {
                sprites[i].DOFade (0, 0);
                posShow[i] = sprites[i].gameObject.transform.localPosition;
                scaleShow[i] = sprites[i].gameObject.transform.localScale;
                sprites[i].gameObject.transform.localPosition = new Vector3 (-0.5f, 0, 0);
                sprites[i].gameObject.transform.localScale = Vector3.zero;
            }
        }

        public override void Show () {
            isShow = true;
            particleAura.emissionRate = 100;
            particleDonut.DOScale (1, 0.7f).SetDelay (0.5f).SetEase (Ease.OutQuart).OnComplete (() => {
                audioSource.Play ();
                particleAura.emissionRate = 0;
                particleGlow.maxParticles = 200;
                pointLight.range = 0.6f;
                DOTween.To (() => _volume, temp => _volume = temp, 1f, 1f);
                // int idx = sprites.Length - 1;
                // for (var i = 0; i < sprites.Length; i++) {
                //     idx = idx - i;

                // }
                for (var i = sprites.Length - 1; i >= 0; i--) {
                    sprites[i].DOFade (1, 1f).SetDelay (0.05f * (sprites.Length - 1 - i)).SetEase (Ease.Linear);
                    sprites[i].gameObject.transform.DOScale (scaleShow[i], 1f).SetDelay (0.05f * (sprites.Length - 1 - i)).SetEase (Ease.OutQuart);
                    sprites[i].gameObject.transform.DOLocalMove (posShow[i], 1f).SetDelay (0.05f * (sprites.Length - 1 - i)).SetEase (Ease.OutQuart);
                }
            });
        }

        public override void Hide () {
            particleAura.emissionRate = 100;
            particleGlow.maxParticles = 0;
            pointLight.range = 0;
            DOTween.To (() => _volume, temp => _volume = temp, 0, 1f).OnComplete (() => {
                isShow = false;
                particleAura.emissionRate = 0;
                audioSource.Stop ();
            });
            for (var i = 0; i < sprites.Length; i++) {
                sprites[i].DOFade (0, 1f).SetDelay (0.02f * i).SetEase (Ease.Linear);
                sprites[i].gameObject.transform.DOScale (new Vector3 (0, 0, 0), 1f).SetDelay (0.02f * i).SetEase (Ease.OutQuart);
                sprites[i].gameObject.transform.DOLocalMove (new Vector3 (-0.5f, 0, 0), 1f).SetDelay (0.02f * i).SetEase (Ease.OutQuart);
            }

            if (isHover)
                HoverOut ();
        }

        public override void HoverIn () {
            if (!isShow) return;
            isHover = true;
            rectImage.GetComponent<Image> ().DOFade (1, _tweenTime).SetEase (Ease.Linear);
            textTitle.DOFade (1, _tweenTime).SetEase (Ease.Linear);

            rectImage.DOLocalMoveY (-30, _tweenTime).SetEase (Ease.OutQuart);
            textTitle.GetComponent<RectTransform> ().DOLocalMoveY (-297, _tweenTime).SetEase (Ease.OutQuart)
                .SetDelay (0.05f);
            rectCoverHolder.DOLocalMoveY (-50, 2f).SetLoops (-1, LoopType.Yoyo).SetEase (Ease.InOutSine);
        }

        public override void HoverOut () {
            isHover = false;
            rectImage.GetComponent<Image> ().DOFade (0, _tweenTime).SetEase (Ease.InOutQuart);
            textTitle.DOFade (0, _tweenTime).SetEase (Ease.InOutQuart);

            rectImage.DOLocalMoveY (-230, _tweenTime).SetEase (Ease.InOutQuart).SetDelay (0.1f);
            textTitle.GetComponent<RectTransform> ().DOLocalMoveY (-458, _tweenTime).SetEase (Ease.InOutQuart)
                .SetDelay (0.05f);
            rectCoverHolder.DOKill ();
            rectCoverHolder.DOLocalMoveY (0, _tweenTime);
        }

        private void Update () {
            if (!isShow) return;
            audioSource.volume = _volume;
        }
    }
}