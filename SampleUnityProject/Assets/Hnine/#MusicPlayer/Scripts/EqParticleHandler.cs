﻿using DG.Tweening;
using UnityEngine;

namespace ARGlass.MusicPlayer
{
    public class EqParticleHandler : MonoBehaviour
    {
        // Start is called before the first frame update
        public float scaleValue;
        public float durationValue;
        public float distanceValue;
        public float alphaValue;
        public SpriteRenderer _spriteRenderer;

        private void Start()
        {
            transform.DOScale(Mathf.Clamp(scaleValue, 0, 0.5f), durationValue).SetEase(Ease.OutSine);
            transform.DOLocalMoveX(distanceValue, durationValue);
            _spriteRenderer.DOFade(alphaValue, durationValue * 0.3f).SetEase(Ease.Linear);
            _spriteRenderer.DOFade(0, durationValue * 0.7f).SetEase(Ease.Linear).SetDelay(durationValue * 0.3f).OnComplete(Destroy);
        }

        private void Destroy()
        {
            Destroy(this.gameObject);
        }
    }
}