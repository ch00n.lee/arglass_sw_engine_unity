﻿using System.Collections;
using System.Collections.Generic;
using ARGlass.MusicPlayer;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class AudioPlayer : MonoBehaviour
{
//    public Text textTime;
    public AudioSource aSource;
//    public Image imgProgress;

    public float[] samples = new float[64];
    public RectTransform[] bars;
    public EqParticleHandler goBass, goTreable;
    public Transform particleHolder;
    public Light PointLight;

    void Start()
    {
    }

    void LateUpdate()
    {
//        textTime.text = GetTime ((int) aSource.time) + " / " + GetTime ((int) aSource.clip.length);
//        imgProgress.fillAmount = aSource.time / aSource.clip.length;
//        
        aSource.GetSpectrumData(samples, 0, FFTWindow.Rectangular);
//        Debug.Log("0 " + (samples[0] * 500));
//        Debug.Log("1 " + (samples[1] * 500));
//        for(var i=0;i< 64; i++)//크기 조절
//        {
//            bars[i].sizeDelta = new Vector2(5, samples[i] * 1000);
//        }
        if (samples[0] * 500 > 495) //bass
        {
            GameObject go = Instantiate(goBass.gameObject, particleHolder, true);
            go.transform.localScale = Vector3.zero;
            go.transform.localPosition = Vector3.zero;
            go.transform.localEulerAngles = new Vector3(0, 90, 0);
            // ReSharper disable once Unity.PerformanceCriticalCodeInvocation
            EqParticleHandler eq = go.GetComponent<EqParticleHandler>();
            // ReSharper disable once Unity.PerformanceCriticalCodeInvocation
            eq._spriteRenderer = go.GetComponent<SpriteRenderer>();
            eq.distanceValue = 0.8f;
            eq.durationValue = 2.2f;
            eq.scaleValue = 0.4f;

            go.SetActive(true);
        }

        if (samples[1] * 500 > 250) //treable
        {
            GameObject go = Instantiate(goTreable.gameObject, particleHolder, true);
            go.transform.localScale = Vector3.zero;
            go.transform.localPosition = Vector3.zero;
            go.transform.localEulerAngles = new Vector3(0, 90, 0);
            // ReSharper disable once Unity.PerformanceCriticalCodeInvocation
            EqParticleHandler eq = go.GetComponent<EqParticleHandler>();
            // ReSharper disable once Unity.PerformanceCriticalCodeInvocation
            eq._spriteRenderer = go.GetComponent<SpriteRenderer>();
            eq.distanceValue = 1.3f;
            eq.durationValue = 1.7f;
            eq.scaleValue = 0.4f;

            go.SetActive(true);
        }

        PointLight.intensity = (samples[0] + samples[1]) / 1.2f;
    }

    string GetTime(int totaltime)
    {
        string returnTime = "";
        int _hour, _min, _second;
        _second = totaltime % 60;
        _min = totaltime / 60;
        _hour = _min / 60;

        string minS, SecS;

        if (_min < 10)
        {
            minS = "0" + _min.ToString();
        }
        else
        {
            minS = _min.ToString();
        }

        if (_second < 10)
        {
            SecS = "0" + _second.ToString();
        }
        else
        {
            SecS = _second.ToString();
        }

        returnTime = minS + ":" + SecS;

        return returnTime;
    }
}