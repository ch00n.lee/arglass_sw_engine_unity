﻿namespace ARGlass
{
    using UnityEngine;

    // Currently this class only supports infinite plane
    public class DetectedPlane : Trackable
    {        
        public Vector3 Center { get; private set; }
        public Vector3 Normal { get; private set; }

        public DetectedPlane()
        {
            SetCenterAndNormal(Vector3.zero, Vector3.up);
            Type = TrackableType.DETECTED_PLANE;
        }

        public DetectedPlane(Vector3 center, Vector3 normal)
        {
            SetCenterAndNormal(center, normal);
            Type = TrackableType.DETECTED_PLANE;
        }

        public void SetPose(Pose pose)
        {
            Pose = pose;
            Center = pose.position;
            Normal = pose.rotation * Vector3.up;
        }

        public void SetCenterAndNormal(Vector3 center, Vector3 normal)
        {
            Center = center;
            Normal = normal;
            Pose = new Pose(center, Quaternion.FromToRotation(Vector3.up, normal));
        }

        public override bool Raycast(Vector3 origin, Vector3 direction, out Vector3 intersection)
        {
            intersection = Vector3.zero;

            float denominator = Vector3.Dot(Normal, direction);
            if(Mathf.Abs(denominator) > 0.0001f)
            {
                float t = Vector3.Dot((Center - origin), Normal) / denominator;
                if(t >= 0)
                {
                    intersection = origin + direction * t;
                    return true;
                }
            }
            return false;
        }

        public override bool Raycast(int screenX, int screenY, out Vector3 intersection)
        {            
            Ray ray = Camera.main.ScreenPointToRay(new Vector3(screenX, screenY, 0));
            return Raycast(ray.origin, ray.direction, out intersection);
        }
    }
}