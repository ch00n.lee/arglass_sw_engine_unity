﻿namespace ARGlass
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Threading;
    using UnityEngine;    

    public class ARGlassController : MonoBehaviour
    {
        public enum TargetDevice
        {
            AR_GLASS,
            MOBILE_PHONE
        }

        public static ARGlassController Instance;
        bool m_IsInitializing = false;
        bool m_IsInitialized = false;
        [SerializeField]
        TargetDevice m_TargetDevice = TargetDevice.AR_GLASS;

        public delegate void InitializationCallback(bool isFinished, string message);
        private InitializationCallback m_InitCallback = null;

        public Texture2D BackgroundTexture { get; private set; } = null;
        public Pose CameraPose { get; private set; }
        public HandGestureRecognitionResult HandGesture { get; private set; } = new HandGestureRecognitionResult();
        public List<Trackable> Trackables { get; private set; } = new List<Trackable>();
        public List<Vector3> PointCloud { get; private set; } = new List<Vector3>();
        public List<GlobalAnchor> GlobalAnchors { get; private set; } = new List<GlobalAnchor>();


        void Awake()
        {
            if(Instance == null)
            {
                Instance = this;
            }
            else if(Instance != null)
            {
                Destroy(gameObject);
            }
        }

        public void Initialize(InitializationCallback initCallback = null)
        {
            if(m_IsInitialized || m_IsInitializing) return;

            m_InitCallback = initCallback;

            switch(m_TargetDevice)
            {
                case TargetDevice.AR_GLASS:                
                    StartCoroutine(_InitializeForARGlass());
                    break;
                case TargetDevice.MOBILE_PHONE:
                    StartCoroutine(_InitializeForMobilePhone());
                    break;
                default:
                    return;
            }

        }

        public void Initialize(TargetDevice targetDevice, InitializationCallback initCallback = null)
        {
            m_TargetDevice = targetDevice;
            Initialize(initCallback);
        }

        public bool IsInitialized()
        {
            return m_IsInitialized;
        }

        public TargetDevice GetTargetDevice()
        {
            return m_TargetDevice;
        }
        
        public void ResetTracking()
        {
            if(!m_IsInitialized) return;

            if(m_TargetDevice == TargetDevice.AR_GLASS)
            {
                ARGlassPluginApi.Instance.ResetTracking();
            }
            else if(m_TargetDevice == TargetDevice.MOBILE_PHONE)
            {
                ARGlassPluginMobilePhoneApi.Instance.ResetTracking();
            }
        }

        public void StartCoSLAM(string ipAddress, int port)
        {
            if(!m_IsInitialized) return;

            if(m_TargetDevice == TargetDevice.AR_GLASS)
            {
                ARGlassPluginApi.Instance.StartCoSLAM(ipAddress, port);
            }
            else if(m_TargetDevice == TargetDevice.MOBILE_PHONE)
            {
                ARGlassPluginMobilePhoneApi.Instance.StartCoSLAM(ipAddress, port);
            }
        }

        public void EnableCoSLAMFusion(bool flag)
        {
            if(!m_IsInitialized) return;

            if(m_TargetDevice == TargetDevice.AR_GLASS)
            {
                ARGlassPluginApi.Instance.EnableCoSLAMFusion(flag);
            }
            else if(m_TargetDevice == TargetDevice.MOBILE_PHONE)
            {
                ARGlassPluginMobilePhoneApi.Instance.EnableCoSLAMFusion(flag);
            }
        }

        public string GetCoSLAMFusionState()
        {
            if(!m_IsInitialized) return "";

            int fusionState;
            switch(m_TargetDevice)
            {
                case TargetDevice.AR_GLASS:
                default:
                    fusionState = ARGlassPluginApi.Instance.GetCoSLAMFusionState();
                    break;
                case TargetDevice.MOBILE_PHONE:
                    fusionState = ARGlassPluginMobilePhoneApi.Instance.GetCoSLAMFusionState();
                    break;
            }

            switch(fusionState)
            {
                case 0:
                    return "Fusion pending";
                case 1:
                    return "Fusion done";
                default:
                    return "Fusion work";
            }
        }

        public string GetCoSLAMSequenceId()
        {
            if(!m_IsInitialized) return "";

            int sequenceId;
            switch(m_TargetDevice)
            {
                case TargetDevice.AR_GLASS:
                default:
                    sequenceId = ARGlassPluginApi.Instance.GetCoSLAMSequenceId();
                    break;
                case TargetDevice.MOBILE_PHONE:
                    sequenceId = ARGlassPluginMobilePhoneApi.Instance.GetCoSLAMSequenceId();
                    break;
            }

            switch(sequenceId)
            {
                case 0:
                    return "None";
                case 1:
                    return "Master";
                default:
                    return "Client " + (sequenceId-1);
            }
        }

        void Update()
        {           
            if(!m_IsInitialized) return;

            _UpdateBackgroundTexture();
            _UpdatePose();
            _UpdateHandGesture();
        }

        void _UpdatePose()
        {            
            float[] cameraPose, planeInfo, pointCloud;
            switch(m_TargetDevice)
            {
                case TargetDevice.AR_GLASS:                
                    ARGlassPluginApi.Instance.GetPose(out cameraPose, out planeInfo, out pointCloud);
                    break;
                case TargetDevice.MOBILE_PHONE:
                    ARGlassPluginMobilePhoneApi.Instance.GetPose(out cameraPose, out planeInfo, out pointCloud);
                    break;
                default:
                    return;
            }

            // If SLAM cannot detect a dominant plane, terminate
            if(pointCloud.Length < 3)
            {
                return;
            }

            // Update camera pose and convert it into Unity's coordinate system
            Quaternion rawRotation = new Quaternion(cameraPose[0], cameraPose[1], cameraPose[2], cameraPose[3]);
            CameraPose = new Pose(new Vector3(cameraPose[4], -cameraPose[5], cameraPose[6]), Quaternion.Euler(-rawRotation.eulerAngles.x, rawRotation.eulerAngles.y, -rawRotation.eulerAngles.z));

            // Update plane (currently, this code only holds the most dominant plane in the current frame)
            Vector3 planeNormal = new Vector3(planeInfo[0], planeInfo[1], planeInfo[2]);
            planeNormal = rawRotation * planeNormal;
            planeNormal.x = -planeNormal.x;
            planeNormal.z = -planeNormal.z;
            Vector3 planeCenter = planeNormal * -planeInfo[3];
            Trackables.Clear();
            Trackables.Add(new DetectedPlane(planeCenter, planeNormal));

            // Update point cloud
            PointCloud.Clear();
            for(int i=0; i<pointCloud.Length/3; ++i)
            {
                PointCloud.Add(new Vector3(pointCloud[i], -pointCloud[i+1], pointCloud[i+2]));
            }
        }

        void _UpdateHandGesture()
        {
            if(m_TargetDevice != TargetDevice.AR_GLASS)
            {
                return;
            }

            HandGestureRecognitionResult result = ARGlassPluginApi.Instance.GetHandGesture();
            if(result != null)
            {
                HandGesture = result;
            }
        }

        void InitializeBackgroundTexture()
        {
            if(m_TargetDevice != TargetDevice.MOBILE_PHONE)
            {
                Debug.LogError("Currently camera image texture is only available for mobile phones");
                return;
            }

            int texPtr = ARGlassPluginMobilePhoneApi.Instance.GetBackgroundTexturePtr();
            if (texPtr == -1)
            {
                Debug.LogError("Cannot get the proper texture address from native plugin");
                return;
            }

            BackgroundTexture = Texture2D.CreateExternalTexture(Frame.CameraImage.Width, Frame.CameraImage.Height, TextureFormat.ARGB32, false, false, (System.IntPtr)texPtr);
        }

        void _UpdateBackgroundTexture()
        {
            if(m_TargetDevice != TargetDevice.MOBILE_PHONE)
            {
                return;
            }

            ARGlassPluginMobilePhoneApi.Instance.UpdateBackgroundTexture();
        }

        void OnApplicationPause(bool pauseStatus)
        {
            if(pauseStatus)
            {
                GazeTracker.Instance.Pause();
            }
            else
            {
                GazeTracker.Instance.Resume();
            }
        }

        void SendInitializationMessage(bool isFinished, string message)
        {
            if(isFinished)
            {
                m_IsInitializing = false;
            }
            
            if(m_InitCallback != null)
            {
                m_InitCallback(isFinished, message);
            }
        }

        IEnumerator _InitializeForARGlass()
        {
            m_IsInitializing = true;
            float timeoutCounter = 0.0f;

            // Check and request permissions for external storage and camera
            if (!UnityEngine.Android.Permission.HasUserAuthorizedPermission(UnityEngine.Android.Permission.ExternalStorageWrite))
            {
                UnityEngine.Android.Permission.RequestUserPermission(UnityEngine.Android.Permission.ExternalStorageWrite);
                SendInitializationMessage(true, "Permission for external storage is required");
                yield break;
            }
            if (!UnityEngine.Android.Permission.HasUserAuthorizedPermission(UnityEngine.Android.Permission.Camera))
            {
                UnityEngine.Android.Permission.RequestUserPermission(UnityEngine.Android.Permission.Camera);
                SendInitializationMessage(true, "Permission for camera is required");
                yield break;
            }

            // Connect IMU and two cameras (RGB and TOF)
            SendInitializationMessage(false, "Try to connect IMU");
            ARGlassPluginApi.Instance.ConnectIMU();
            while(!ARGlassPluginApi.Instance.IsIMUConnected())
            {
                timeoutCounter += Time.deltaTime;                
                yield return null;

                if(timeoutCounter > 2.0f)
                {
                    SendInitializationMessage(true, "IMU connection timed out - you may need to kill and restart the app");
                }
            }

            SendInitializationMessage(false, "Try to connect Camera #1");
            ARGlassPluginApi.Instance.ConnectCamera();
            yield return new WaitForSeconds(1.0f);

            SendInitializationMessage(false, "Try to connect Camera #2");
            ARGlassPluginApi.Instance.ConnectCamera();
            yield return new WaitForSeconds(1.0f);

            // Initialize the video streams
            SendInitializationMessage(false, "Try to initialize video stream");
            if(!ARGlassPluginApi.Instance.InitializeVideoStream())
            {
                SendInitializationMessage(true, "Failed to initialize the video stream");
                yield break;
            }
            else
            {
                // TODO: we need a function / flag to check whether the images are ready or not
                yield return new WaitForSeconds(2.0f);
            }

            // Start SLAM
            SendInitializationMessage(false, "Try to start tracking");
            ARGlassPluginApi.Instance.StartTracking();
            yield return new WaitForSeconds(1.0f);

            // Initialize a gaze tracker
            SendInitializationMessage(false, "Try to initialize a gaze tracker");
            GazeTracker.Instance.Initialize();
            yield return new WaitForSeconds(1.0f);

            m_IsInitialized = true;            
            SendInitializationMessage(true, "Success to initialize the AR glass");
        }

        IEnumerator _InitializeForMobilePhone()
        {
            m_IsInitializing = true;

            // Check and request permissions for external storage and camera
            if (!UnityEngine.Android.Permission.HasUserAuthorizedPermission(UnityEngine.Android.Permission.ExternalStorageWrite))
            {
                UnityEngine.Android.Permission.RequestUserPermission(UnityEngine.Android.Permission.ExternalStorageWrite);
                SendInitializationMessage(true, "Permission for external storage is required");
                yield break;
            }
            if (!UnityEngine.Android.Permission.HasUserAuthorizedPermission(UnityEngine.Android.Permission.Camera))
            {
                UnityEngine.Android.Permission.RequestUserPermission(UnityEngine.Android.Permission.Camera);
                SendInitializationMessage(true, "Permission for camera is required");
                yield break;
            }

            SendInitializationMessage(false, "Try to start tracking");
            ARGlassPluginMobilePhoneApi.Instance.StartTracking();
            yield return new WaitForSeconds(1.0f);

            SendInitializationMessage(false, "Try to get the camera image texture");
            InitializeBackgroundTexture();
            yield return new WaitForSeconds(1.0f);

            m_IsInitialized = true;
            SendInitializationMessage(true, "Success to initialize SLAM on mobile phone");
        }
    }
}