﻿namespace ARGlass
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;    

    // Currently this class only supports infinite plane
    public class Trackable
    {
        public enum TrackableType
        {
            DETECTED_PLANE
        };

        public TrackableType Type { get; protected set; }
        public Pose Pose { get; protected set; }
        public List<Anchor> Anchors { get; protected set; } = new List<Anchor>();

        public virtual bool Raycast(Vector3 origin, Vector3 direction, out Vector3 intersection)
        {
            intersection = Vector3.zero;
            switch(Type)
            {
                case TrackableType.DETECTED_PLANE:
                    return ((DetectedPlane)this).Raycast(origin, direction, out intersection);
                default:
                    return false;
            }
        }

        public virtual bool Raycast(int screenX, int screenY, out Vector3 intersection)
        {            
            Ray ray = Camera.main.ScreenPointToRay(new Vector3(screenX, screenY, 0));
            return Raycast(ray.origin, ray.direction, out intersection);
        }

        public Anchor CreateAnchor(Pose localPose, string objectName = "Anchor")
        {
            GameObject anchorObj = new GameObject(objectName);            
            Anchor anchor = anchorObj.AddComponent<Anchor>();
            anchor.Parent = this;
            anchor.LocalPose = localPose;
            anchor.Update();
            Anchors.Add(anchor);
            return anchor;
        }

        public bool RemoveAnchor(Anchor anchor)
        {
            int index = Anchors.IndexOf(anchor);
            if(index < 0)
            {
                return false;
            }

            Anchors.RemoveAt(index);
            Object.Destroy(anchor.gameObject);
            return true;
        }
    }
}