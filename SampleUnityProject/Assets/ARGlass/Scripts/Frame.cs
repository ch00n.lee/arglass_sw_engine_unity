﻿namespace ARGlass
{
    using System.Collections.Generic;
    using UnityEngine;

    public static class Frame
    {
        public static CameraImage CameraImage
        {
            get
            {
                return CameraImage.Instance;
            }
        }

        public static Pose Pose
        {
            get
            {                
                return ARGlassController.Instance.CameraPose;
            }
        }

        public static List<Trackable> Trackables
        {
            get
            {
                return ARGlassController.Instance.Trackables;
            }
        }

        public static List<Vector3> PointCloud
        {
            get
            {
                return ARGlassController.Instance.PointCloud;
            }
        }

        public static HandGestureRecognitionResult HandGesture
        {
            get
            {
                if(!ARGlassController.Instance.IsInitialized() || ARGlassController.Instance.GetTargetDevice() != ARGlassController.TargetDevice.AR_GLASS)
                {
                    return new HandGestureRecognitionResult();
                }
                return ARGlassController.Instance.HandGesture;
            }
        }

        public static List<GlobalAnchor> GlobalAnchors
        {
            get
            {
                return ARGlassController.Instance.GlobalAnchors;
            }
        }

        public static float[] GazeData
        {
            get
            {
                return GazeTracker.Instance.GetGazeData();
            }
        }

        public static List<TrackableHit> RaycastAll(Vector3 origin, Vector3 direction)
        {
            List<TrackableHit> trackableHit = new List<TrackableHit>();

            for(int i=0; i<ARGlassController.Instance.Trackables.Count; ++i)
            {
                Vector3 intersection;
                if(ARGlassController.Instance.Trackables[i].Raycast(origin, direction, out intersection))
                {                    
                    trackableHit.Add(new TrackableHit(ARGlassController.Instance.Trackables[i], intersection, ARGlassController.Instance.Trackables[i].Pose.rotation));
                }
            }

            return trackableHit;
        }

        public static List<TrackableHit> RaycastAll(int screenX, int screenY)
        {            
            Ray ray = Camera.main.ScreenPointToRay(new Vector3(screenX, screenY, 0));
            return RaycastAll(ray.origin, ray.direction);
        }
    }
}