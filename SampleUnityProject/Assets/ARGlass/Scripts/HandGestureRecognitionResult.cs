﻿namespace ARGlass
{
    public class HandGestureRecognitionResult
    {
        public bool IsLeftHandDetected = false;
        public bool IsRightHandDetected = false;
        public string LeftHandGesture = "";
        public string RightHandGesture = "";

        // Bounding box: x, y, width, height
        public float[] LeftHandBoundingBox = new float[4];
        public float[] RightHandBoundingBox = new float[4];

        // Skeletion: 21 * 3 (x, y, z)
        // May need to convert the coordinate system
        public float[] LeftSkeleton3D = new float[63];
        public float[] RightSkeleton3D = new float[63];

        // 3D Position: x, y, z
        // May need to convert the coordinate system
        public float[] LeftHandCenter3D = new float[3];
        public float[] RightHandCenter3D = new float[3];
    }
}