﻿namespace ARGlass
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class CameraImage
    {
        private static CameraImage m_Instance;
        public static CameraImage Instance
        {
            get
            {
                if(m_Instance == null)
                {
                    m_Instance = new CameraImage();
                }
                return m_Instance;
            }
        }

        public int Width { get; } = 640;
        public int Height { get; } = 480;

        public Texture2D Texture
        {
            get
            {
                return ARGlassController.Instance.BackgroundTexture;
            }
        }

        private Vector2 GetFocalLength()
        {
            Vector2 result;
            float screenRatio;
            switch(UnityEngine.Input.deviceOrientation)
            {
                case DeviceOrientation.LandscapeLeft:                
                case DeviceOrientation.LandscapeRight:
                default:
                    screenRatio = (float)Screen.height / (float)Screen.width;
                    result.y = 3.26522f;
                    result.x = result.y * screenRatio;
                    break;
                case DeviceOrientation.Portrait:
                case DeviceOrientation.PortraitUpsideDown:
                    screenRatio = (float)Screen.width / (float)Screen.height;
                    result.x = 3.26522f;
                    result.y = result.x * screenRatio;
                    break;
            }
            return result;
        }

        public Matrix4x4 GetProjectionMatrix(float near, float far)
        {
            Vector2 focalLength = GetFocalLength();

            // Assume that camera parameters are symmetric
            Matrix4x4 result = Matrix4x4.zero;
            result.m00 = focalLength.x;
            result.m11 = focalLength.y;
            result.m22 = -(far + near) / (far - near);
            result.m23 = -(2.0f * far * near) / (far - near);
            result.m32 = -1.0f;
            return result;
        }
        
        public void GetTextureUV(out Vector4 topLeftRight, out Vector4 bottomLeftRight)
        {
            topLeftRight = new Vector4(0, 1, 1, 1);
            bottomLeftRight = new Vector4(0, 0, 1, 0);

            float cameraImageRatio = (float)Height / (float)Width;
            float screenRatio, offset;            

            // We will not consider some unusual devices which have different orientation schemes rather than typical mobile phones
            // We assume that (image.height / image.width) is always smaller than (screen.height / screen.width)
            switch(UnityEngine.Input.deviceOrientation)
            {
                // Crop top and bottom
                case DeviceOrientation.LandscapeLeft:
                default:
                    screenRatio = (float)Screen.height / (float)Screen.width;
                    offset =  (1.0f - screenRatio / cameraImageRatio) * 0.5f;
                    topLeftRight = new Vector4(0, 1-offset, 1, 1-offset);
                    bottomLeftRight = new Vector4(0, offset, 1, offset);
                    break;
                case DeviceOrientation.LandscapeRight:                
                    screenRatio = (float)Screen.height / (float)Screen.width;
                    offset =  (1.0f - screenRatio / cameraImageRatio) * 0.5f;
                    topLeftRight = new Vector4(1, offset, 0, offset);
                    bottomLeftRight = new Vector4(1, 1-offset, 0, 1-offset);
                    break;
                // Crop left and right
                case DeviceOrientation.Portrait:
                    screenRatio = (float)Screen.width / (float)Screen.height;
                    offset =  (1.0f - screenRatio / cameraImageRatio) * 0.5f;
                    topLeftRight = new Vector4(1, 1-offset, 1, offset);
                    bottomLeftRight = new Vector4(0, 1-offset, 0, offset);
                    break;
                case DeviceOrientation.PortraitUpsideDown:
                    screenRatio = (float)Screen.width / (float)Screen.height;
                    offset =  (1.0f - screenRatio / cameraImageRatio) * 0.5f;
                    topLeftRight = new Vector4(0, offset, 0, 1-offset);
                    bottomLeftRight = new Vector4(1, offset, 1, 1-offset);
                    break;
            }
        }
    }
}