﻿namespace ARGlass
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class ARGlassPluginMobilePhoneApi
    {
        private static ARGlassPluginMobilePhoneApi m_Instance;
        public static ARGlassPluginMobilePhoneApi Instance
        {
            get
            {
                if(m_Instance == null)
                {
                    m_Instance = new ARGlassPluginMobilePhoneApi();
                }
                return m_Instance;
            }
        }

        AndroidJavaObject m_CurrentActivity;
        AndroidJavaObject m_PluginObject;

        private ARGlassPluginMobilePhoneApi()
        {
            m_CurrentActivity = (new AndroidJavaClass("com.unity3d.player.UnityPlayer")).GetStatic<AndroidJavaObject>("currentActivity");
            m_PluginObject = (new AndroidJavaClass("com.samsung.unityphoneplugin.UnityPlugin")).CallStatic<AndroidJavaObject>("getInstance");
            m_PluginObject.Call<bool>("init", m_CurrentActivity);
        }

        public void StartTracking()
        {
            m_PluginObject.Call("startSLAM");
        }

        public void StartCoSLAM(string ipAddress, int port)
        {
            m_PluginObject.Call("startCoSLAM", ipAddress, port);
        }

        public void ResetTracking()
        {
            m_PluginObject.Call("reset");
        }

        public void GetPose(out float[] cameraPose, out float[] planeInfo, out float[] pointCloud)
        {            
            AndroidJavaObject poseData = m_PluginObject.Call<AndroidJavaObject>("getPose");

            if(poseData == null)
            {
                cameraPose = new float[7];
                planeInfo = new float[4];
                pointCloud = new float[0];
                return;
            }

            cameraPose = poseData.Get<float[]>("cameraPose");
            planeInfo = poseData.Get<float[]>("planeInfo");
            pointCloud = poseData.Get<float[]>("pointCloud");
        }

        public void EnableCoSLAMFusion(bool flag)
        {
            m_PluginObject.Call("enableFusion", flag);
        }

        public int GetCoSLAMSequenceId()
        {
            return m_PluginObject.Call<int>("getSequenceId");
        }

        public int GetCoSLAMFusionState()
        {
            return m_PluginObject.Call<int>("getFusionState");
        }

        public int GetBackgroundTexturePtr()
        {
            return m_PluginObject.Call<int>("getBackgroundTexture");
        }

        public void UpdateBackgroundTexture()
        {
            m_PluginObject.Call("updateBackgroundTexture");
        }
    }
}