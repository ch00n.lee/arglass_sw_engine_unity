﻿namespace ARGlass
{    
    using UnityEngine;

    // Attach this component to the main camera
    public class TrackedPoseDriver : MonoBehaviour
    {
        private Camera m_Camera;

        void Start()
        {
            m_Camera = GetComponent<Camera>();
        }

        void Update()
        {
            m_Camera.projectionMatrix = CameraImage.Instance.GetProjectionMatrix(m_Camera.nearClipPlane, m_Camera.farClipPlane);
            transform.localPosition = ARGlassController.Instance.CameraPose.position;
            transform.localRotation = ARGlassController.Instance.CameraPose.rotation;
        }
    }
}