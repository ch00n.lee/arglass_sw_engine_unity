﻿namespace ARGlass
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class GlobalAnchor : MonoBehaviour
    {
        public void AddChildObject(GameObject obj)
        {
            obj.transform.parent = transform;
        }

        public void AddChildObject(GameObject obj, Vector3 localPosition, Quaternion localRotation)
        {
            obj.transform.parent = transform;
            obj.transform.localPosition = localPosition;
            obj.transform.localRotation = localRotation;            
        }
    }
}