﻿namespace ARGlass
{
    using UnityEngine;

    public class TrackableHit
    {
        public Trackable Trackable { get; private set; }
        public Pose Pose { get; private set; }

        public TrackableHit(Trackable trackable, Pose pose)
        {
            Trackable = trackable;
            Pose = pose;
        }

        public TrackableHit(Trackable trackable, Vector3 position, Quaternion rotation)
        {
            Trackable = trackable;
            Pose = new Pose(position, rotation);
        }
    }
}


