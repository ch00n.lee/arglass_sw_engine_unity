using System.Collections.Generic;
using UnityEngine;
namespace AdhawkAndroidBackend
{
    /*
        Proxy class representing the adhawk unity api used to pipe tracker data, acknowledgements, errors and camera data
        to the unity client
     */
    class AdhawkUnityCallback : AndroidJavaProxy
    {        
        private int DATA_TYPE_FLOAT;
        private const string FLOAT_DATA_CLASS_STR = "com.android.adhawk.adhawk_public_api.messaging.FloatData";
        private AndroidJavaClass FloatData;
        private int DATA_TYPE_INT;
        private const string INT_DATA_CLASS_STR = "com.android.adhawk.adhawk_public_api.messaging.IntegerData";
        private AndroidJavaClass IntData;
        private int DATA_TYPE_STRING;
        private const string STRING_DATA_CLASS_STR = "com.android.adhawk.adhawk_public_api.messaging.StringData";
        private AndroidJavaClass StringData;
        private int DATA_TYPE_BYTES;
        private const string BYTE_DATA_CLASS_STR = "com.android.adhawk.adhawk_public_api.messaging.ByteData";
        private AndroidJavaClass ByteData;

        private delegate void Parser(sbyte type, AndroidJavaObject packet, bool stream);

        private Dictionary<int, Parser> _mPacketParsingDict;

        public AdhawkUnityCallback() : base("com.android.adhawk.adhawk_public_api.unity_port.IAHUnityApi")
        {            
            DATA_TYPE_FLOAT = AdhawkMessaging.DATA_TYPE_FLOAT;
            DATA_TYPE_INT = AdhawkMessaging.DATA_TYPE_INT;
            DATA_TYPE_BYTES = AdhawkMessaging.DATA_TYPE_BYTES;
            DATA_TYPE_STRING = AdhawkMessaging.DATA_TYPE_STRING;

            _mPacketParsingDict = new Dictionary<int, Parser>();
            _mPacketParsingDict.Add(DATA_TYPE_FLOAT, parseFloatArray);
            _mPacketParsingDict.Add(DATA_TYPE_INT, parseIntArray);
            _mPacketParsingDict.Add(DATA_TYPE_BYTES, parseByteArray);
            _mPacketParsingDict.Add(DATA_TYPE_STRING, parseString);

            FloatData = new AndroidJavaClass(FLOAT_DATA_CLASS_STR);
            IntData = new AndroidJavaClass(INT_DATA_CLASS_STR);
            ByteData = new AndroidJavaClass(BYTE_DATA_CLASS_STR);
            StringData = new AndroidJavaClass(STRING_DATA_CLASS_STR);
        }

        public void handleStream(sbyte type, AndroidJavaObject packet)
        {
            int dataType = packet.Call<int>("getDataType");
            using(AndroidJavaObject packetData = packet.Call<AndroidJavaObject>("getData"))
            {
                if(packetData.GetRawObject().ToInt32() != 0)
                {
                    _mPacketParsingDict[dataType]?.Invoke(type, packetData, true);
                }
            }
        }

        public void handleResponse(sbyte type, AndroidJavaObject packet)
        {
            int dataType = packet.Call<int>("getDataType");
            using(AndroidJavaObject packetData = packet.Call<AndroidJavaObject>("getData"))
            {
                if(packetData.GetRawObject().ToInt32() != 0)
                {
                    _mPacketParsingDict[dataType]?.Invoke(type, packetData, false);
                }
            }
        }

        public void handleAck(sbyte type, int code)
        {
            PacketFactory.SharedInstance().handleAck(type, code);
        }

        public void onFrame(AndroidJavaObject frame)
        {
            if(frame.GetRawObject().ToInt32() != 0)
            {
                PacketFactory.SharedInstance().handleCameraFrame(AndroidJNIHelper.ConvertFromJNIArray<byte[]>(frame.GetRawObject()));
            }
        }

        private void parseString(sbyte type, AndroidJavaObject data, bool stream)
        {
            AndroidJavaObject stringData = StringData.CallStatic<AndroidJavaObject>("cast", data);
            if(stream)
            {
                PacketFactory.SharedInstance().handleStream(type, stringData.Call<string>("getData"));
            }
            else
            {
                PacketFactory.SharedInstance().handleResponse(type, stringData.Call<string>("getData"));
            }
        }   

        private void parseFloatArray(sbyte type, AndroidJavaObject data, bool stream)
        {
            AndroidJavaObject floatData = FloatData.CallStatic<AndroidJavaObject>("cast", data);
            AndroidJavaObject javaFloatArray = floatData.Call<AndroidJavaObject>("getData");
            if(stream)
            {
                PacketFactory.SharedInstance().handleStream(type, AndroidJNIHelper.ConvertFromJNIArray<float[]>(javaFloatArray.GetRawObject()));
            }
            else
            {
                PacketFactory.SharedInstance().handleResponse(type, AndroidJNIHelper.ConvertFromJNIArray<float[]>(javaFloatArray.GetRawObject()));
            }
        }

        private void parseIntArray(sbyte type, AndroidJavaObject data, bool stream)
        {
            AndroidJavaObject intData = IntData.CallStatic<AndroidJavaObject>("cast", data);
            AndroidJavaObject javaIntegerArray = intData.Call<AndroidJavaObject>("getData");
            if(stream)
            {
                PacketFactory.SharedInstance().handleStream(type, AndroidJNIHelper.ConvertFromJNIArray<int[]>(javaIntegerArray.GetRawObject()));
            }
            else
            {
                PacketFactory.SharedInstance().handleResponse(type, AndroidJNIHelper.ConvertFromJNIArray<int[]>(javaIntegerArray.GetRawObject()));
            }
        }

        private void parseByteArray(sbyte type, AndroidJavaObject data, bool stream)
        {
            AndroidJavaObject byteData = ByteData.CallStatic<AndroidJavaObject>("cast", data);
            AndroidJavaObject javaByteArray = byteData.Call<AndroidJavaObject>("getData");
            if(stream)
            {
                PacketFactory.SharedInstance().handleStream(type, AndroidJNIHelper.ConvertFromJNIArray<byte[]>(javaByteArray.GetRawObject()));
            }
            else
            {
                PacketFactory.SharedInstance().handleResponse(type, AndroidJNIHelper.ConvertFromJNIArray<byte[]>(javaByteArray.GetRawObject()));
            }
        }
    }
}