using UnityEngine;
namespace AdhawkAndroidBackend
{
    /*
        Helper class containing constants and handler definitions used by the unity backend library and clients
    */
    public class AHConstants
    {
        // Logcat debugging tag
        public const string TAG = "AHUNITY_ABACKEND";
        public delegate void GazeDataHandler(float timestamp, float x, float y, float z);
        public delegate void GlintDataHandler(float timestamp, float x, float y, int pdIndex, int trackerId);
        public delegate void AutotunePositionHandler(int tracker1X, int tracker1Y, int tracker2X, int tracker2Y);
        public delegate void CameraFrameHandler(byte[] frame);
        public delegate void AckHandler(sbyte type, int code);
        public delegate void ErrorHandler(sbyte type, int error);
    }
}