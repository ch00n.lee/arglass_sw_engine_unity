﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using AdhawkAndroidBackend;
/*
    Sample script showing how to register a gaze data handler and begin the data stream
 */
public class AdhawkTrackerScript : MonoBehaviour
{
    
    private float[] _mGazeData = new float[4];
    private float[] _mGlintData = new float[5];
    //TextMesh _mTextMesh;
    Text _mText;
    private bool _mRegistered = false;

    private bool _mStreamStarted = false;
    // Start is called before the first frame update
    void Start()
    {
        AdhawkPublicApi.SharedInstance().RegisterGazeDataHandler(gazeDataHandler);
        AdhawkPublicApi.SharedInstance().RegisterGlintDataHandler(glintDataHandler);
        AdhawkPublicApi.SharedInstance().RegisterAckHandler(ackHandler);
        AdhawkPublicApi.SharedInstance().Start();
        //_mTextMesh = GetComponent<TextMesh>();
        _mText = GetComponent<Text>();
    }

    void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus && _mStreamStarted && _mRegistered)
        {
            AdhawkPublicApi.SharedInstance().StopGazeDataStream();
            AdhawkPublicApi.SharedInstance().StopGlintDataStream();
            _mStreamStarted = false;
            Debug.LogError("On Pause Stop");
        }
        else
        {
            if(!_mStreamStarted && _mRegistered)
            {
                // AdhawkPublicApi.SharedInstance().SetStreamRate(120);
                _mStreamStarted = true;
                AdhawkPublicApi.SharedInstance().StartGazeDataStream();
                AdhawkPublicApi.SharedInstance().StartGlintDataStream();
                Debug.LogError("On Resume Start");
            }
        }
    }

    void ackHandler(sbyte type, int code)
    {
        if(type == AdhawkMessaging.COMMAND_USB_DEVICE_CONNECTED)
        {
            _mRegistered = true;
            AdhawkPublicApi.SharedInstance().StartGazeDataStream();
            AdhawkPublicApi.SharedInstance().StartGlintDataStream();
        }
        Debug.LogError("Ack for: " + type + " with code: " + code);
    }

    void gazeDataHandler(float ts, float x, float y, float z)
    {
        _mGazeData[0] = ts;
        _mGazeData[1] = x;
        _mGazeData[2] = y;
        _mGazeData[3] = z;
    }

    void glintDataHandler(float ts, float x, float y, int pid, int tid)
    {
        _mGlintData[0] = ts;
        _mGlintData[1] = x;
        _mGlintData[2] = y;
        _mGlintData[3] = pid;
        _mGlintData[4] = tid;
    }

    // Update is called once per frame
    void Update()
    {
        string msg = "Gaze Data: ";
        msg += string.Join(",", _mGazeData);
        msg += "\n";
        msg += "Glint Data: ";
        msg += string.Join(",", _mGlintData);

        //_mTextMesh.text = msg;
        _mText.text = msg;
    }
}
