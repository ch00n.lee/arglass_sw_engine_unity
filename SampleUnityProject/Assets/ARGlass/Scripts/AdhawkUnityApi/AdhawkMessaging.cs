using UnityEngine;
namespace AdhawkAndroidBackend
{
    /*
        Helper class used to pull messaging constants like command ids and data types from android library
     */
    public class AdhawkMessaging
    {
        private const string ADHAWK_MESSAGING_CONSTANTS_CLASS = "com.android.adhawk.adhawk_public_api.messaging.AdHawkMessages";
        private static AndroidJavaClass _mAdhawkMessaging = new AndroidJavaClass(ADHAWK_MESSAGING_CONSTANTS_CLASS);

        public static int DATA_TYPE_FLOAT
        {
            get
            {
                return _mAdhawkMessaging.GetStatic<int>("DATA_TYPE_FLOAT");
            }
        }

        public static int DATA_TYPE_INT
        {
            get
            {
                return _mAdhawkMessaging.GetStatic<int>("DATA_TYPE_INT");
            }
        }

        public static int DATA_TYPE_BYTES
        {
            get
            {
                return _mAdhawkMessaging.GetStatic<int>("DATA_TYPE_BYTES");
            }
        }

        public static int DATA_TYPE_STRING
        {
            get
            {
                return _mAdhawkMessaging.GetStatic<int>("DATA_TYPE_STRING");
            }
        }

        public static sbyte COMMAND_USB_DEVICE_CONNECTED
        {
            get
            {
                return _mAdhawkMessaging.GetStatic<sbyte>("COMMAND_USB_DEVICE_CONNECTED");
            }
        }

        public static sbyte COMMAND_REGISTER_CALLBACK
        {
            get
            {
                return _mAdhawkMessaging.GetStatic<sbyte>("COMMAND_REGISTER_CALLBACK");
            }
        }

        public static sbyte COMMAND_UNREGISTER_CALLBACK
        {
            get
            {
                return _mAdhawkMessaging.GetStatic<sbyte>("COMMAND_UNREGISTER_CALLBACK");
            }
        }

        public static sbyte COMMAND_CALIBRATION_START
        {
            get
            {
                return _mAdhawkMessaging.GetStatic<sbyte>("COMMAND_CALIBRATION_START");
            }
        }

        public static sbyte COMMAND_CALIBRATION_COMPLETE
        {
            get
            {
                return _mAdhawkMessaging.GetStatic<sbyte>("COMMAND_CALIBRATION_COMPLETE");
            }
        }

        public static sbyte COMMAND_CALIBRATION_ABORT
        {
            get
            {
                return _mAdhawkMessaging.GetStatic<sbyte>("COMMAND_CALIBRATION_ABORT");
            }
        }

        public static sbyte COMMAND_CALIBRATION_READ
        {
            get
            {
                return _mAdhawkMessaging.GetStatic<sbyte>("COMMAND_CALIBRATION_READ");
            }
        }

        public static sbyte COMMAND_CALIBRATION_REGISTER
        {
            get
            {
                return _mAdhawkMessaging.GetStatic<sbyte>("COMMAND_CALIBRATION_REGISTER");
            }
        }

        public static sbyte COMMAND_AUTOTUNE_TRIGGER
        {
            get
            {
                return _mAdhawkMessaging.GetStatic<sbyte>("COMMAND_AUTOTUNE_TRIGGER");
            }
        }

        public static sbyte COMMAND_AUTOTUNE_READ_POSITION
        {
            get
            {
                return _mAdhawkMessaging.GetStatic<sbyte>("COMMAND_AUTOTUNE_READ_POSITION");
            }
        }

        public static sbyte COMMAND_START_STREAM
        {
            get
            {
                return _mAdhawkMessaging.GetStatic<sbyte>("COMMAND_START_STREAM");
            }
        }

        public static sbyte COMMAND_START_VIDEO
        {
            get
            {
                return _mAdhawkMessaging.GetStatic<sbyte>("COMMAND_START_VIDEO");
            }
        }

        public static string COMMAND_ID_TO_STRING(int id)
        {
            return _mAdhawkMessaging.CallStatic<string>("COMMAND_ID_TO_STR", new object[]{id});
        }

        public static int STREAM_GAZE_DATA
        {
            get
            {
                return _mAdhawkMessaging.GetStatic<int>("STREAM_GAZE_DATA");
            }
        }

        public static int STREAM_GLINT_DATA
        {
            get
            {
                return _mAdhawkMessaging.GetStatic<int>("STREAM_GLINT_DATA");
            }
        }

        public static int STREAM_DIAGNOSTIC_DATA
        {
            get
            {
                return _mAdhawkMessaging.GetStatic<int>("STREAM_DIAGNOSTIC_DATA");
            }
        }

        public int STREAM_AUTOTUNE_COMPLETE
        {
            get
            {
                return _mAdhawkMessaging.GetStatic<int>("STREAM_AUTOTUNE_COMPLETE");
            }
        }

        public static int READ_AUTOTUNE_DATA
        {
            get
            {
                return _mAdhawkMessaging.GetStatic<int>("READ_AUTOTUNE_DATA");
            }
        }

        public static int READ_CALIBRATION_DATA
        {
            get
            {
                return _mAdhawkMessaging.GetStatic<int>("READ_CALIBRATION_DATA");
            }
        }
    }
}