using System;
using UnityEngine;

namespace AdhawkAndroidBackend
{
    /*
        Class used to invoke data handlers registered by unity client to distribute tracker data
     */
    internal class PacketFactory
    {
        // Required function signature of gaze data handler callback
        internal AHConstants.GazeDataHandler _mGazeDataHandlers;        
        internal AHConstants.GlintDataHandler _mGlintDataHandlers;
        internal AHConstants.AutotunePositionHandler _mAutotunePositionHandlers;
        internal AHConstants.CameraFrameHandler _mCameraFrameHandlers;
        internal AHConstants.AckHandler _mAckHandlers;

        private static PacketFactory _mInstance;

        private PacketFactory()
        {}

        internal static PacketFactory SharedInstance()
        {
            if(_mInstance == null)
            {
                _mInstance = new PacketFactory();
            }
            return _mInstance;
        }

        internal void handleStream(sbyte type, float[] data)
        {
            if(type == AdhawkMessaging.STREAM_GAZE_DATA)
            {
                _mGazeDataHandlers?.Invoke(data[0], data[1], data[2], data[3]);
            }
            else if(type == AdhawkMessaging.STREAM_GLINT_DATA)
            {
                _mGlintDataHandlers?.Invoke(data[0], data[1], data[2], (int)data[3], (int)data[4]);
            }
        }

        internal void handleStream(sbyte type, int[] data)
        {
            if(type == AdhawkMessaging.READ_AUTOTUNE_DATA)
            {
                _mAutotunePositionHandlers?.Invoke(data[0], data[1], data[2], data[3]);
            }
        }

        internal void handleStream(sbyte type, byte[] data)
        {
            
        }

        internal void handleStream(sbyte type, string data)
        {
        }

        //Handle Response
        internal void handleResponse(sbyte type, float[] data)
        {
        }

        internal void handleResponse(sbyte type, int[] data)
        {
        }

        internal void handleResponse(sbyte type, byte[] data)
        {
        }

        internal void handleResponse(sbyte type, string data)
        {
        }

        internal void handleAck(sbyte type, int code)
        {
            _mAckHandlers?.Invoke(type, code);
        }

        internal void handleCameraFrame(byte[] frame)
        {
            _mCameraFrameHandlers?.Invoke(frame);
        }
    }
}