using System.Threading;
using UnityEngine;

namespace AdhawkAndroidBackend
{
    /**
        Unity wrapper of Adhawk service api created in the adhawk_public_api library
     */
    public class AdhawkPublicApi
    {
        private static AdhawkPublicApi _mInstance;
        private AndroidJavaClass _mAdhawkUnityPlayerClass;
        private AndroidJavaObject _mCurrentActivity;

        private AdhawkUnityCallback _mAdhawkUnityCallback;
        public const string TRACKER_ACTIVITY_CLASS = "com.android.adhawk.adhawk_public_api.unity_port.AdHawkUnityTrackerActivity";
 
        private AdhawkPublicApi()
        {
            _mAdhawkUnityCallback = new AdhawkUnityCallback();
            _mAdhawkUnityPlayerClass = new AndroidJavaClass(TRACKER_ACTIVITY_CLASS);
            _mCurrentActivity = (new AndroidJavaClass("com.unity3d.player.UnityPlayer")).GetStatic<AndroidJavaObject>("currentActivity");
            _mAdhawkUnityPlayerClass.CallStatic("registerCallback", new object[]{_mAdhawkUnityCallback});
        }

        public static AdhawkPublicApi SharedInstance()
        {
            if(_mInstance == null)
            {
                _mInstance = new AdhawkPublicApi();
            }
            return _mInstance;
        }

        public void Start()
        {
            _mCurrentActivity.Call("attachTrackerService");
        }

        public void StartGazeDataStream()
        {
            _mCurrentActivity?.Call("StartGazeDataStream");
        }

        public void StopGazeDataStream()
        {
            _mCurrentActivity?.Call("StopGazeDataStream");
        }

        public void StartGlintDataStream()
        {
            _mCurrentActivity?.Call("StartGlintDataStream");
        }

        public void StopGlintDataStream()
        {
            _mCurrentActivity?.Call("StopGlintDataStream");
        }

        public void SetStreamRate(float rate)
        {
            _mCurrentActivity?.Call("SetStreamRate", rate);
        }

        public void CalibrationStart()
        {
            _mCurrentActivity?.Call("CalibrationStart");
        }

        public void CalibrationComplete()
        {
            _mCurrentActivity?.Call("CalibrationComplete");
        }

        public void CalibrationAbort()
        {
            _mCurrentActivity?.Call("CalibrationAbort");
        }

        public void RegisterCalibrationDataPoint(float x, float y, float z)
        {
            _mCurrentActivity?.Call("RegisterCalibrationDataPoint", new object[]{x, y, z});
        }

        public void ReadCalibrationData()
        {
            _mCurrentActivity?.Call("ReadCalibrationData");
        }

        public void TriggerAutotune()
        {
            _mCurrentActivity?.Call("TriggerAutotune");
        }

        public void ReadAutotunePosition()
        {
            _mCurrentActivity?.Call("ReadAutotunePosition");
        }

        public void StartVideoStream()
        {
            _mCurrentActivity?.Call("StartVideoStream");
        }

        public void RegisterGazeDataHandler(AHConstants.GazeDataHandler handler)
        {
            PacketFactory.SharedInstance()._mGazeDataHandlers += handler;
        }

        public void UnregisterGazeDataHandler(AHConstants.GazeDataHandler handler)
        {
            PacketFactory.SharedInstance()._mGazeDataHandlers -= handler;
        }

        public void RegisterGlintDataHandler(AHConstants.GlintDataHandler handler)
        {
            PacketFactory.SharedInstance()._mGlintDataHandlers += handler;
        }

        public void UnregisterGlintDataHandler(AHConstants.GlintDataHandler handler)
        {
            PacketFactory.SharedInstance()._mGlintDataHandlers -= handler;
        }

        public void RegisterAckHandler(AHConstants.AckHandler handler)
        {
            PacketFactory.SharedInstance()._mAckHandlers += handler;
        }

        public void UnregisterAckHandler(AHConstants.AckHandler handler)
        {
            PacketFactory.SharedInstance()._mAckHandlers -= handler;
        }

        public void RegisterAutotunePositionHandler(AHConstants.AutotunePositionHandler handler)
        {
            PacketFactory.SharedInstance()._mAutotunePositionHandlers += handler;
        }

        public void UnregisterAutotunePositionHandler(AHConstants.AutotunePositionHandler handler)
        {
            PacketFactory.SharedInstance()._mAutotunePositionHandlers -= handler;
        }

        public void RegisterCameraFrameHandler(AHConstants.CameraFrameHandler handler)
        {
            PacketFactory.SharedInstance()._mCameraFrameHandlers += handler;
        }

        public void UnregisterCameraFrameHandler(AHConstants.CameraFrameHandler handler)
        {
            PacketFactory.SharedInstance()._mCameraFrameHandlers -= handler;
        }
    }
}