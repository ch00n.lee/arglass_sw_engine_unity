﻿namespace ARGlass
{    
    using UnityEngine;
    using AdhawkAndroidBackend;

    public class GazeTracker
    {
        private static GazeTracker m_Instance;
        public static GazeTracker Instance
        {
            get
            {
                if(m_Instance == null)
                {
                    m_Instance = new GazeTracker();
                }
                return m_Instance;
            }
        }

        private float[] m_GazeData = new float[4];
        private float[] m_GlintData = new float[5];

        private bool m_Registered = false;
        private bool m_StreamStarted = false;

        public void Initialize()
        {
            AdhawkPublicApi.SharedInstance().RegisterGazeDataHandler(gazeDataHandler);
            AdhawkPublicApi.SharedInstance().RegisterGlintDataHandler(glintDataHandler);
            AdhawkPublicApi.SharedInstance().RegisterAckHandler(ackHandler);
            AdhawkPublicApi.SharedInstance().Start();
        }

        public void Pause()
        {
            if (m_StreamStarted && m_Registered)
            {
                AdhawkPublicApi.SharedInstance().StopGazeDataStream();
                AdhawkPublicApi.SharedInstance().StopGlintDataStream();
                m_StreamStarted = false;
                Debug.Log("[GazeTracker] On Pause Stop");
            }
        }

        public void Resume()
        {
            if (!m_StreamStarted && m_Registered)
            {
                m_StreamStarted = true;
                AdhawkPublicApi.SharedInstance().StartGazeDataStream();
                AdhawkPublicApi.SharedInstance().StartGlintDataStream();
                Debug.Log("[GazeTracker] On Resume Start");
            }
        }

        public float[] GetGazeData()
        {
            return m_GazeData;
        }

        public float[] GetGlintData()
        {
            return m_GlintData;
        }

        public bool IsStreamStarted()
        {
            return m_StreamStarted;
        }

        void ackHandler(sbyte type, int code)
        {
            if (type == AdhawkMessaging.COMMAND_USB_DEVICE_CONNECTED)
            {
                m_Registered = true;
                AdhawkPublicApi.SharedInstance().StartGazeDataStream();
                AdhawkPublicApi.SharedInstance().StartGlintDataStream();
            }
            Debug.Log("[GazeTracker] Ack for: " + type + " with code: " + code);
        }

        void gazeDataHandler(float ts, float x, float y, float z)
        {
            m_GazeData[0] = ts;
            m_GazeData[1] = x;
            m_GazeData[2] = y;
            m_GazeData[3] = z;
        }

        void glintDataHandler(float ts, float x, float y, int pid, int tid)
        {
            m_GlintData[0] = ts;
            m_GlintData[1] = x;
            m_GlintData[2] = y;
            m_GlintData[3] = pid;
            m_GlintData[4] = tid;
        }
    }
}
