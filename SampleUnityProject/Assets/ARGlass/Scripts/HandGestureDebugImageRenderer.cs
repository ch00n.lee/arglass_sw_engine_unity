﻿namespace ARGlass
{
    using UnityEngine;
    using UnityEngine.UI;

    public class HandGestureDebugImageRenderer : MonoBehaviour
    {
        [Tooltip("Need to flip horizontally by setting the 'H' field as -1 in the 'UV Rect' tab")]
        public RawImage SkeletonRawImage;

        bool m_IsSkeletonTextureReady = false;
        bool m_IsUpdating = true;

        void InitializeSkeletonTexture()
        {
            if (m_IsSkeletonTextureReady) return;

            if (SkeletonRawImage == null) return;

            if(ARGlassController.Instance.GetTargetDevice() != ARGlassController.TargetDevice.AR_GLASS) return;

            if (!ARGlassController.Instance.IsInitialized()) return;

            int texPtr = ARGlassPluginApi.Instance.GetSkeletonTexturePtr();
            if (texPtr == -1) return;

            // To avoid "ArgumentException: nativeTex can not be null," need to disable "Multithreaded Rendering" in Player Settings
            //
            // Note that the Unity's texture coordinate is flipped horizontally from OpenGL ES's one
            // So you need to filp the texture coordinate of RawImage component to correctly visualize the image (UV Rect -> H = -1)
            Texture2D tex = Texture2D.CreateExternalTexture(224, 171, TextureFormat.ARGB32, false, false, (System.IntPtr)texPtr);
            SkeletonRawImage.texture = tex;

            m_IsSkeletonTextureReady = true;
        }

        void Update()
        {
            if (m_IsUpdating)
            {
                if (!m_IsSkeletonTextureReady) InitializeSkeletonTexture();
                if (m_IsSkeletonTextureReady) ARGlassPluginApi.Instance.UpdateSkeletonTexture();
            }
        }

        void OnEnable()
        {
            m_IsUpdating = true;
        }

        void OnDisable()
        {
            m_IsUpdating = false;
        }
    }
}