﻿namespace ARGlass
{
    using UnityEngine;

    public class Anchor : MonoBehaviour
    {
        public Trackable Parent;
        public Pose LocalPose;

        public void Update()
        {
            transform.position = Parent.Pose.position + Parent.Pose.rotation * LocalPose.position;
            transform.rotation = Parent.Pose.rotation * LocalPose.rotation;
        }

        public void AddChildObject(GameObject obj)
        {
            obj.transform.parent = transform;
        }

        public void AddChildObject(GameObject obj, Vector3 localPosition, Quaternion localRotation)
        {
            obj.transform.parent = transform;
            obj.transform.localPosition = localPosition;
            obj.transform.localRotation = localRotation;            
        }
    }
}
