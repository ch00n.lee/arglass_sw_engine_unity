﻿namespace ARGlass
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.XR;

    public class BackgroundRenderer : MonoBehaviour
    {
        private Camera m_Camera;
        private ARBackgroundRenderer m_BackgroundRenderer;
        public Material BackgroundMaterial;

        void Start()
        {            
        }

        void OnEnable()
        {
            if (m_BackgroundRenderer == null)
            {
                m_BackgroundRenderer = new ARBackgroundRenderer();
            }

            if(BackgroundMaterial == null)
            {
                Debug.LogError("No background material is assigned");
            }
            
            m_Camera = GetComponent<Camera>();
            m_BackgroundRenderer.camera = m_Camera;
            m_BackgroundRenderer.backgroundMaterial = BackgroundMaterial;
            m_BackgroundRenderer.mode = ARRenderMode.MaterialAsBackground;
        }

        void OnDisable()
        {
            if (m_BackgroundRenderer != null)
            {
                m_BackgroundRenderer.mode = ARRenderMode.StandardBackground;
                m_BackgroundRenderer.camera = null;
            }
        }

        void Update()
        {
            if(!ARGlassController.Instance.IsInitialized())
            {
                return;
            }

            if(ARGlassController.Instance.GetTargetDevice() != ARGlassController.TargetDevice.MOBILE_PHONE)
            {
                Debug.LogError("Currently background renderer is only available for mobile phones");
                this.enabled = false;
                return;
            }
            
            _UpdateBackgroundMaterial();
        }

        void _UpdateBackgroundMaterial()
        {
            Vector4 topLeftRight, bottomLeftRight;
            CameraImage.Instance.GetTextureUV(out topLeftRight, out bottomLeftRight);

            BackgroundMaterial.SetTexture("_MainTex", ARGlassController.Instance.BackgroundTexture);
            BackgroundMaterial.SetVector("_UvTopLeftRight", topLeftRight);
            BackgroundMaterial.SetVector("_UvBottomLeftRight", bottomLeftRight);
        }
    }
}


