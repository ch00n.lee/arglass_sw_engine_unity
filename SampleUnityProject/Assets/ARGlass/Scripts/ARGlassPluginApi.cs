﻿namespace ARGlass
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class ARGlassPluginApi
    {
        private static ARGlassPluginApi m_Instance;
        public static ARGlassPluginApi Instance
        {
            get
            {
                if(m_Instance == null)
                {
                    m_Instance = new ARGlassPluginApi();
                }
                return m_Instance;
            }
        }

        AndroidJavaObject m_CurrentActivity;
        AndroidJavaObject m_PluginObject;


        private ARGlassPluginApi()
        {
            m_CurrentActivity = (new AndroidJavaClass("com.unity3d.player.UnityPlayer")).GetStatic<AndroidJavaObject>("currentActivity");
            m_PluginObject = (new AndroidJavaClass("com.samsung.unityplugin.UnityPlugin")).CallStatic<AndroidJavaObject>("getInstance");
            m_PluginObject.Call<bool>("init", m_CurrentActivity);
        }

        public void ConnectIMU()
        {            
            m_PluginObject.Call("connectIMU");
        }
        public void ConnectCamera()
        {
            m_PluginObject.Call("setupCamera");
        }
        public bool InitializeVideoStream()
        {
            return !m_PluginObject.Call<bool>("initVideoStream", true);
        }
        public void StartTracking()
        {
            m_PluginObject.Call("launch");
        }

        public void ResetTracking()
        {
            m_PluginObject.Call("reset");
        }

        public bool IsIMUConnected()
        {
            return m_PluginObject.Call<bool>("isIMUConnected");
        }

        public void StartCoSLAM(string ipAddress, int port)
        {
            m_PluginObject.Call("startCoslam", ipAddress, port);
        }

        public void EnableCoSLAMFusion(bool flag)
        {
            m_PluginObject.Call("enableFusion", flag);
        }

        public int GetCoSLAMSequenceId()
        {
            int[] state = m_PluginObject.Call<int[]>("updateFusionInfo");
            return state[1];
        }

        public int GetCoSLAMFusionState()
        {
            int[] state = m_PluginObject.Call<int[]>("updateFusionInfo");
            return state[0];
        }

        public void GetPose(out float[] cameraPose, out float[] planeInfo, out float[] pointCloud)
        {            
            AndroidJavaObject poseData = m_PluginObject.Call<AndroidJavaObject>("getPose");

            if(poseData == null)
            {
                cameraPose = new float[7];
                planeInfo = new float[4];
                pointCloud = new float[0];
                return;
            }

            cameraPose = poseData.Get<float[]>("cameraPose");
            planeInfo = poseData.Get<float[]>("planeInfo");
            pointCloud = poseData.Get<float[]>("pointCloud");
        }

        public HandGestureRecognitionResult GetHandGesture()
        {
            HandGestureRecognitionResult result = new HandGestureRecognitionResult();
            AndroidJavaObject nativeResult = m_PluginObject.Call<AndroidJavaObject>("run");

            if(nativeResult == null)
            {
                return null;
            }

            // For acquiring more detailed information from the native plugin, refer to the GestureRecogResult class in GestureEnginePlugin.java
            result.IsLeftHandDetected = nativeResult.Get<bool>("isLeftHandDetected");
            result.IsRightHandDetected = nativeResult.Get<bool>("isRightHandDetected");
            result.LeftHandGesture = nativeResult.Get<string>("leftHandGesture");
            result.RightHandGesture = nativeResult.Get<string>("rightHandGesture");
            result.LeftHandBoundingBox = nativeResult.Get<float[]>("leftHandBBox");
            result.RightHandBoundingBox = nativeResult.Get<float[]>("rightHandBBox");
            result.LeftSkeleton3D = nativeResult.Get<float[]>("leftSkeleton3D");
            result.RightSkeleton3D = nativeResult.Get<float[]>("rightSkeleton3D");
            result.LeftHandCenter3D = nativeResult.Get<float[]>("leftHandCenter3D");
            result.RightHandCenter3D = nativeResult.Get<float[]>("rightHandCenter3D");

            return result;
        }

        public int GetSkeletonTexturePtr()
        {
            return m_PluginObject.Call<int>("getSkeletonTexturePtr");
        }

        public void UpdateSkeletonTexture()
        {
            m_PluginObject.Call("updateSkeletonTexture");
        }
    }
}