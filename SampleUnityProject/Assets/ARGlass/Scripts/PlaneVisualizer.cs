﻿namespace ARGlass
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    // Attach this component to the plane mesh
    public class PlaneVisualizer : MonoBehaviour
    {
        [Tooltip("Input plane's width and height in meter")]
        public float PlaneSize = 0.4f;

        void Start()
        {
            Hide();
        }
        
        void Update()
        {
            if (!ARGlassController.Instance.IsInitialized())
            {
                return;
            }

            if(Frame.Trackables.Count < 1)
            {
                Hide();
            }
            else
            {
                Show();
                Vector3 intersection;
                if(Frame.Trackables[0].Raycast(Camera.main.transform.position, Camera.main.transform.forward, out intersection))
                {
                    transform.position = intersection;
                }
                else
                {
                    transform.position = Frame.Trackables[0].Pose.position;
                }                
                transform.rotation = Frame.Trackables[0].Pose.rotation;
            }
        }

        void Show()
        {
            transform.localScale = new Vector3(PlaneSize * 0.1f, 1.0f, PlaneSize * 0.1f);
        }

        void Hide()
        {
            transform.localScale = Vector3.zero;
        }
    }
}
