﻿Shader "ARGlass/Background"
{
    Properties
    {        
        _MainTex ("Main Texture", 2D) = "black" {}
        _UvTopLeftRight ("UV of top corners", Vector) = (0, 1, 1, 1)
        _UvBottomLeftRight ("UV of bottom corners", Vector) = (0, 0, 1, 0)
    }
    SubShader
    {
        ZWrite Off

        Pass
		{
			CGPROGRAM            

            #pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

            sampler2D _MainTex;
			float2 _MainTex_TexelSize;

            uniform float4 _UvTopLeftRight;
            uniform float4 _UvBottomLeftRight;
			
			struct v2f
			{
				float4 pos : POSITION;
				float2 uv : TEXCOORD0;
			};

			v2f vert(appdata_base v)
			{
                float2 uvTop = lerp(_UvTopLeftRight.xy, _UvTopLeftRight.zw, v.texcoord.x);
                float2 uvBottom = lerp(_UvBottomLeftRight.xy, _UvBottomLeftRight.zw, v.texcoord.x);

				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = lerp(uvTop, uvBottom, v.texcoord.y);

				return o;
			}

			half4 frag(v2f i) : COLOR
			{
                return tex2D(_MainTex, i.uv);
			}

			ENDCG
		}
    }
}
