# ARGlass SW Engine for Unity

## 개요
각종 Unity 기반 서비스에서 AR Glass를 활용하기 위한 통합 API
* /SampleUnityProject: AR Glass 통합 플러그인 및 예시 Unity 프로젝트
    - /SampleUnityProject/Assets/ARGlass/Scripts: AR Glass 구동 및 제어를 위한 스크립트
        + /SampleUnityProject/Assets/ARGlass/Scripts/AdhawkUnityApi: 시선 추적 플러그인 스크립트
    - /SampleUnityProject/Assets/ARGlass/Prefabs: Unity Scene에 추가하여 사용할 수 있는 Prefab
    - /SampleUnityProject/Assets/Plugins/Android: 네이티브 플러그인 바이너리 파일 및 AndroidManifest.xml 파일
* 기존 Unity 프로젝트에 Import하여 사용하실 수 있는 패키지(.unitypackage)는 본 Repository의 Release 페이지에서 다운로드가 가능합니다.
    - https://github.sec.samsung.net/RS8-ARGlass-SW/ARGlass_SW_Engine_Unity/releases
<br/><br/>

## 들어가기 전에
* 현재 AR Glass 하드웨어를 초기화하고 제어하는 부분이 다소 불안정한 상태이며 플러그인 내에서도 미구현된 부분들이 존재합니다. 따라서 현재의 버전은 향후 개발을 위한 API 참고용으로만 사용하여 주시기 바라며 기능이 안정화되는 대로 업데이트된 버전을 Release할 예정입니다.
* 본 플러그인의 다른 구성 요소는 Unity 2018.3 이상의 버전에서 정상적으로 작동하는 것을 확인하였으나, 시선 추적 플러그인은 Unity **2019.1** 이상의 버전에서만 동작합니다. 만약 시선 추적 기능을 사용하시고자 할 경우에는 개발 환경을 Unity 2019.1 이상의 버전으로 준비하여 주시기 바랍니다.
* 하기 API 규격은 지속적으로 확장될 예정이며 추후 상황에 따라 변경될 수 있습니다.
<br/><br/>

## 전체 구조
![Overview](./images/overview.png)
<br/><br/>

## 클래스
* 플러그인에 포함된 클래스는 크게 Unity의 MonoBehavior를 상속받아 Scene에 추가될 수 있는 클래스와 다른 클래스의 내부에서 참조되는 클래스로 구분됩니다.
    - 현재 ARGlassController와 TrackedPoseDriver, PlaneVisualizer 클래스 등이 MonoBehavior를 상속받는 형태로 정의되어 있으며, 이 중에서 **ARGlassController**와 **TrackedPoseDriver**는 Scene에 필수적으로 포함되어야 합니다. (자세한 사항은 각 클래스 설명 및 플러그인 사용 방법을 참고하여 주시기 바랍니다.)
* 모든 클래스는 **ARGlass** 네임스페이스 하에 있습니다.
* 본 문서에서는 분량 관계상 서비스 개발 시 직접 접근이 필요한 함수 및 필드에 대해서만 설명을 드리며, 기타 내부 함수에 관해서는 필요하실 경우 패키지에 포함된 코드를 참고하여 주시기 바랍니다.

---

### ARGlassPluginApi
* AR Glass 활용을 위한 네이티브 플러그인을 초기화하고 다른 클래스의 필요에 따라 각 네이티브 함수를 호출하는 기능을 담당하는 클래스입니다.
* 다른 클래스에 노출되어 있지만 개발 시 본 클래스에 직접 접근하실 필요는 없습니다.

---

### ARGlassPluginMobilePhoneApi
* ARGlassPluginApi와 역할은 유사하나 AR Glass 대신 스마트폰 상에서 SLAM을 구동하기 위한 클래스입니다.
* 초기화 시 Target Device를 어느 쪽으로 선택하느냐에 따라 내부적으로 ARGlassPluginApi 또는 ARGlassPluginMobilePhoneApi 중 하나가 연결됩니다.
* 다른 클래스에 노출되어 있지만 개발 시 본 클래스에 직접 접근하실 필요는 없습니다.

---

### ARGlassController
* 본 API의 진입점으로서 ARGlassPluginApi 클래스에 정의된 함수들을 이용하여 AR Glass를 초기화하고 데이터를 획득하는 역할을 담당합니다.
* Scene 상에 활성화되어 있는 임의의 오브젝트에 컴포넌트로 추가하고 Initialize 함수를 호출하면 AR Glass의 각 모듈들에 대한 초기화를 수행하며 이후 매 프레임마다 공간 정보 및 제스처 등의 데이터를 갱신합니다.
* 본 클래스에 있는 데이터 필드는 직접 접근도 가능하지만 관리의 일관성을 위해 가급적 Frame 클래스를 통해 접근하여 주시기 바랍니다.

#### public void *Initialize*(InitializationCallback initCallback = null)
#### public void Initialize(TargetDevice targetDevice, InitializationCallback initCallback = null)
* AR Glass를 초기화합니다.
    - 본래 각 모듈별로 분리되어 있는 초기화 과정을 단일 함수로 처리하기 위해 내부에서 이들을 Coroutine으로 묶고 각 단계별로 약간의 Wait를 주었습니다. 이를 통해 네이티브 플러그인 내에서 비동기적으로 처리되는 일부 초기화 루틴이 완전히 종료될 때까지 대기 시간을 확보하여 이전 단계가 완료되지 않은 상태에서 다음 단계로 진입 시 발생하는 오류를 최대한 방지하고자 하였습니다. 하지만 상황에 따라 대기 시간이 충분치 못하거나 하드웨어와의 연결이 불안정하여 오류가 발생할 가능성이 존재하며 이러한 부분은 추후 지속적으로 개선해 나갈 예정입니다.
    - 파라미터로 **void function(bool isFinished, string message)** 형태를 가지는 콜백 함수를 전달할 수 있으며, 이 경우 콜백 함수를 통해 초기화 진행 단계별로 종료 여부(isFinished)와 진행 상황 메시지(message)를 확인할 수 있습니다. (ExampleUsage.cs 스크립트 참고)
* 초기화 시 스크립트 상에서 Target Device를 명시적으로 지정할 수도 있으며 혹은 Inspector에서 선택할 수도 있습니다.
* 초기화가 이미 완료되었거나 진행 중인 경우에는 실행되지 않습니다.

#### public bool *IsInitialized*()
* 초기화가 정상적으로 완료된 경우 true를 반환합니다. 단, 초기화 성공 여부가 통합 플러그인 구동에 영향을 미치지 않는 시선 추적 모듈은 별도로 성공 여부를 체크하지 않습니다.
* Initialize 함수에 파라미터로 전달되는 콜백 함수와 함께 사용하여 초기화가 종료된 직후 초기화가 성공했는지의 여부를 판단하고 적절한 후속 조치를 취할 수 있습니다.

#### public TargetDevice *GetTargetDevice*()
* 현재 설정된 Target Device를 반환합니다.

#### public void *ResetTracking*()
* 현재 동작 중인 SLAM 모듈을 초기화하고 현재의 위치를 원점으로 하여 다시 Tracking을 수행합니다.
* 주의: 현재는 정상적으로 초기화가 되지 않는 경우가 있습니다.

#### public void *StartCoSLAM*(string ipAddress, int port)
* 지정된 IP 및 포트 번호를 이용하여 CoSLAM 서버에 접속합니다.
* 초기화 이후에 실행되어야 합니다.

#### public void *EnableCoSLAMFusion*(bool flag)
* StartCoSLAM을 실행한 후 실제로 Map Fusion에 들어가기 위한 함수입니다.
* 인자로 true를 입력할 경우 Map Fusion이 시작되며 false를 입력할 경우 중지됩니다.

#### public string *GetCoSLAMFusionState*()
* CoSLAM의 Map Fusion 상태를 문자열로 반환합니다.

#### public string *GetCoSLAMSequenceId*()
* CoSLAM의 Sequence ID(Master 혹은 Client #)를 문자열로 반환합니다.

---

### TrackedPoseDriver
* AR Glass의 SLAM 모듈로부터 획득한 Pose 및 RGB 카메라의 Projection Matrix를 Unity의 렌더링용 카메라에 적용하는 역할을 담당합니다.
* Scene의 Main Camera에 컴포넌트로 추가하면 AR Glass가 초기화된 이후 매 프레임마다 자동적으로 Pose를 카메라의 Transform에 적용하여 사용자의 시점과 카메라의 시점을 일치시킵니다.

---

### BackgroundRenderer
* Scene의 Main Camera에 컴포넌트로 추가하면 RGB 카메라로부터 입력된 영상을 배경에 표시하여 줍니다. (직접 추가 시 Inspector에서 BackgroundMaterial 연결 필요)
* 스마트폰용 API에서만 동작하며 ARGlassController Prefab을 추가하시면 추가적인 세팅 없이 프로젝트에서 바로 사용이 가능합니다.
* Multithreaded Rendering을 사용 중일 경우에는 정상적으로 작동하지 않기 때문에 빌드 시 'Player Settings' -> 'Other Settings'에서 Multithreaded Rendering 옵션을 해제해 주셔야 합니다.

---

### CameraImage
* 배경 렌더링을 위한 Projection Matrix 및 화면비에 따른 적절한 UV 좌표를 계산하는 클래스입니다.
    - Projection Matrix의 경우 현재 N10+ 카메라의 수치가 하드코딩되어 있으며 추후 지원 기종을 추가할 예정입니다.
* 개발 시 본 클래스에 직접 접근할 필요는 없습니다.

---

### GazeTracker
* Adhawk에서 제공한 Unity용 시선 추적 플러그인의 Wrapper 클래스입니다.
* 초기화 및 데이터 갱신은 ARGlassController를 통해 이루어지며 데이터에 대한 접근은 Frame 클래스를 통해 가능하기 때문에 개발 시 본 클래스에 직접 접근하실 필요는 없습니다.

---

### Trackable
* 실제 공간으로부터 검출된 공간 정보(평면, 특징점 등)를 통칭하는 부모 클래스입니다.
* 현재는 자식 클래스로 DetectedPlane만을 가지고 있습니다.

#### public TrackableType *Type*
* 해당 Trackable의 상세한 타입입니다.
* 현재는 DetectedPlane(TrackableType.DETECTED_PLANE)만이 존재합니다.

#### public Pose *Pose*
* 해당 Trackable의 Pose입니다.

#### public List<Anchor> *Anchors*
* 해당 Trackable에 속한 Anchor의 리스트입니다.

#### public virtual bool *Raycast*(Vector3 origin, Vector3 direction, out Vector3 intersection)
* 해당 Trackable과의 Raycast 결과를 반환합니다.
* 파라미터로는 3차원 공간상에서 Ray의 시작점(origin) 및 방향 벡터(direction)를 받습니다.
* 실행 결과로서 Ray와의 교차 여부를 반환하며 교차점의 3차원 좌표는 intersection 파라미터를 통해 반환합니다.
* 일반적인 상황에서는 본 함수보다는 Frame 클래스의 RaycastAll 함수를 사용할 가능성이 높을 것으로 생각됩니다.

#### public virtual bool *Raycast*(int screenX, int screenY, out Vector3 intersection)
* 해당 Trackable과의 Raycast 결과를 반환합니다.
* 파라미터로는 2D 스크린 좌표(screenX, screenY)를 받습니다.
* 실행 결과로서 Ray와의 교차 여부를 반환하며 교차점의 3차원 좌표는 intersection 파라미터를 통해 반환합니다.
* 일반적인 상황에서는 본 함수보다는 Frame 클래스의 RaycastAll 함수를 사용할 가능성이 높을 것으로 생각됩니다.

#### public Anchor *CreateAnchor*(Pose localPose, string objectName = "Anchor")
* 해당 Trackable에 종속된 Anchor를 생성하고 이를 GameObject로 Scene에 추가하며 생성된 Anchor의 인스턴스를 반환합니다.
* 파라미터 중 localPose는 해당 Trackable과의 상대적 Pose를 의미하며, objectName은 생성될 GameObject의 이름을 의미합니다.

#### public void *RemoveAnchor*(Anchor anchor)
* 지정된 Anchor가 해당 Trackable에 속해 있을 경우 Anchor의 GameObject를 삭제하고 true를 반환합니다.
* 만약 지정된 Anchor가 해당 Trackable에 속해 있지 않을 경우 false를 반환하며 Anchor는 삭제되지 않습니다.

---

### DetectedPlane
* SLAM 및 Plane Detector로부터 검출된 평면을 정의하는 클래스로서 Trackable을 상속한 자식 클래스입니다.
* Y축 양의 방향을 기본 Up Vector로 간주합니다. (Unity 기본 설정)
* 주의: 현 시점에서 일부 기능은 미구현 상태이며 Plane Detector가 별도로 추가되지 않은 상태에서는 현재 공간에서 가장 Dominant한 하나의 무한 평면만을 도출합니다.

#### public Vector3 *Center*
* 해당 평면 상에 위치한 한 점의 3차원 좌표입니다.

#### public Vector3 *Normal*
* 해당 평면의 법선 벡터입니다.

#### public void *SetPose*(Pose pose)
* 해당 평면의 Pose를 정의합니다. 이때 입력되는 Pose에 따라 Center와 Normal은 자동적으로 결정됩니다.

#### public void *SetCenterAndNormal*(Vector3 center, Vector3 normal)
* 해당 평면의 Center와 Normal을 정의합니다. 이때 입력되는 Center와 Normal에 따라 Pose는 자동적으로 결정됩니다.

#### public override bool *Raycast*(Vector3 origin, Vector3 direction, out Vector3 intersection)
* 해당 평면과의 Raycast 결과를 반환합니다.
* 파라미터로는 3차원 공간상에서 Ray의 시작점(origin) 및 방향 벡터(direction)를 받습니다.
* 실행 결과로서 Ray와의 교차 여부를 반환하며 교차점의 3차원 좌표는 intersection 파라미터를 통해 반환합니다.
* 일반적인 상황에서는 본 함수보다는 Frame 클래스의 RaycastAll 함수를 사용할 가능성이 높을 것으로 생각됩니다.

#### public override bool *Raycast*(int screenX, int screenY, out Vector3 intersection)
* 해당 평면과의 Raycast 결과를 반환합니다.
* 파라미터로는 2D 스크린 좌표(screenX, screenY)를 받습니다.
* 실행 결과로서 Ray와의 교차 여부를 반환하며 교차점의 3차원 좌표는 intersection 파라미터를 통해 반환합니다.
* 일반적인 상황에서는 본 함수보다는 Frame 클래스의 RaycastAll 함수를 사용할 가능성이 높을 것으로 생각됩니다.

---

### Anchor
* 특정 Trackable을 기준으로 오브젝트를 배치(예: 검출된 평면 상에 배치된 가상 오브젝트)하기 위한 상대적 위치를 정의하는 클래스입니다.
* 주의: 현 시점에서는 Trackable이 지속적으로 유지되지 않기 때문에 Anchor 대신 GlobalAnchor를 사용하시기 바랍니다.

#### public Trackable *Parent*
* 해당 Anchor가 속한 Trackable입니다.

#### public Pose *LocalPose*
* 기준이 되는 Trackable로부터의 상대적 Pose입니다.

#### public void *AddChildObject*(GameObject obj)
#### public void *AddChildObject*(GameObject obj, Vector3 localPosition, Quaternion localRotation)
* 지정된 GameObject(obj)를 해당 Anchor의 Child로 추가합니다.

---

### GlobalAnchor
* Anchor와 유사한 역할을 하나 특정 Trackable에 종속되지 않고 World 좌표계 상에 직접 위치하는 Anchor입니다.
* 현재 Trackable이 지속적으로 유지되지 않기 때문에 Anchor가 필요하실 경우에는 대신 이 GlobalAnchor를 사용하시기 바랍니다.

#### public void *AddChildObject*(GameObject obj)
#### public void *AddChildObject*(GameObject obj, Vector3 localPosition, Quaternion localRotation)
* 지정된 GameObject(obj)를 해당 Anchor의 Child로 추가합니다.

---

### TrackableHit
* Frame 클래스를 통해 RaycastAll을 수행하였을 때 결과를 전달하기 위한 클래스입니다.

#### public Trackable *Trackable*
* Ray와 교차가 발생한 Trackable의 인스턴스입니다.

#### public Pose *Pose*
* Ray와 교차가 발생한 지점의 3차원 좌표 및 Orientation입니다.

---

### HandGestureRecognitionResult
* Hand Gesture의 검출 결과를 저장 및 전달하기 위한 클래스입니다.
* 기본적으로 Hand Gesture 인식 플러그인의 결과 클래스 구조를 동일하게 사용하고 있습니다.

#### public bool *IsLeftHandDetected*
* 왼손의 검출 여부입니다.

#### public bool *IsRightHandDetected*
* 오른손의 검출 여부입니다.

#### public string *LeftHandGesture*
* 인식된 왼손 제스처의 명칭입니다.

#### public string *RightHandGesture*
* 인식된 오른손 제스처의 명칭입니다.

---

### Frame
* AR Glass의 각 모듈로부터 획득한 데이터를 통합하여 서비스에 전달해주는 역할을 담당하는 클래스입니다.
* 모든 함수 및 필드가 Static으로 정의되어 있어서 인스턴스화 없이 바로 호출하시면 됩니다.

#### public static CameraImage *CameraImage*
* CameraImage 객체에 접근할 수 있습니다..

#### public static Pose *Pose*
* SLAM 모듈로부터 계산된 최신 Pose를 획득할 수 있습니다.
* 주의: 사용 단말 및 환경에 따라 SLAM으로부터 정확한 Pose가 도출되지 않을 수도 있습니다.

#### public static List<Vector3> *PointCloud*
* SLAM으로부터 도출된 Feature Point의 리스트를 획득할 수 있습니다.

#### public static List<GlobalAnchor> *GlobalAnchors*
* 현재 정의되어 있는 GlobalAnchor의 리스트를 획득할 수 있습니다.

#### public static HandGestureRecognitionResult *HandGesture*
* 최신 Hand Gesture 인식 결과를 획득할 수 있습니다.
* 주의: 현재 초기 버전의 Hand Gesture 플러그인이 탑재되어 있으며 영상 품질 등의 Issue로 인해 정확한 Gesture가 도출되지 않을 수 있습니다.

#### public static float[] *GazeData*
* 시선 추적 모듈을 사용 가능할 경우 최신 시선 추적 결과를 획득할 수 있습니다.
* 데이터는 float[4] 형식이며 Index 0부터 순서대로 Timestamp와 x, y, z값을 의미합니다.

#### public static List<TrackableHit> *RaycastAll*(Vector3 origin, Vector3 direction)
* 현재 공간상에서 검출된 모든 Trackable과의 Raycast 결과를 반환합니다.
* 파라미터로는 3차원 공간상에서 Ray의 시작점(origin) 및 방향 벡터(direction)를 받습니다.
* 주의: 현재 평면을 포함한 Trackable의 검출이 이루어지지 않는 관계로 사용이 불가합니다.

#### public static List<TrackableHit> *RaycastAll*(int screenX, int screenY)
* 현재 공간상에서 검출된 모든 Trackable과의 Raycast 결과를 반환합니다.
* 파라미터로는 2D 스크린 좌표(screenX, screenY)를 받습니다.
* 주의: 현재 평면을 포함한 Trackable의 검출이 이루어지지 않는 관계로 사용이 불가합니다.

---

### PlaneVisualizer
* 검출된 평면의 위치를 렌더링하기 위한 클래스입니다.
* Unity의 Plane(또는 평면 표시를 위한 다른 오브젝트)에 컴포넌트로 추가하면 검출된 평면의 Pose에 맞추어 해당 오브젝트가 Scene에 표시됩니다.
* PlaneVisualizer Prefab을 이용하시면 Scene에 이를 간편하게 추가하실 수 있습니다.
* 주의: 현재 SLAM은 가장 Dominant한 하나의 무한 평면만을 반환하기 때문에 PlaneVisualizer는 해당 평면상의 일부 영역을 표시합니다.

---

### HandGestureDebugImageRenderer
* Hand Gesture 플러그인으로부터 디버그용 Skeleton 이미지를 Texture2D의 형태로 전달받아 이를 RawImage 상에 표시하기 위한 클래스입니다.
* 사용을 위해서는 RawImage 오브젝트가 필요하며, HandGestureDebugImageRenderer를 해당 RawImage 또는 기타 적당한 오브젝트에 컴포넌트로 추가한 후 해당 RawImage를 HandGestureDebugImageRenderer의 skeletonRawImage 필드에 연결합니다.
    - 직접 Scene에 추가하는 것보다 미리 구성된 HandSkeletonRawImage Prefab을 사용하시는 것이 편리합니다.
* Scene에 정상적으로 추가될 경우 추가적인 스크립팅 없이 ARGlassController가 초기화되면 자동적으로 이미지 출력을 시작하며 Disable될 경우에는 갱신을 멈춥니다.
* OpenGL ES와 Unity의 텍스처 좌표계가 Flip되어 있는 관계로 출력을 위한 UI 요소(RawImage 등)의 UV Rect 중 H를 -1로 설정하여 이를 다시 뒤집어야 합니다.
* Multithreaded Rendering을 사용 중일 경우에는 정상적으로 작동하지 않기 때문에 빌드 시 'Player Settings' -> 'Other Settings'에서 Multithreaded Rendering 옵션을 해제해 주셔야 합니다.
<br/><br/>

## Prefab
* 현재 패키지에는 세 종류의 Prefab이 포함되어 있으며 추후 각종 편의 기능들을 Prefab화하여 추가할 예정입니다.

### ARGlassController
* 플러그인의 진입점 및 렌더링용 카메라 등 Scene을 구성하는 필수 요소들을 포함한 Prefab입니다.
* 새로운 프로젝트나 기존 프로젝트에서 플러그인을 적용할 경우 본 Prefab을 Scene에 추가하고 원래 있던 Main Camera를 제거하시면 됩니다.

### PlaneVisualizer
* SLAM으로부터 검출된 평면을 시각화하기 위한 Prefab입니다.
* 자세한 사항은 PlaneVisualizer 클래스의 설명을 참고하여 주시기 바랍니다.

### HandSkeletonRawImage
* HandGestureDebugImageRenderer와 RawImage가 결합되어 있는 Prefab으로서 Hand Gesture 인식 결과를 확인할 수 있는 이미지를 UI 상에 출력합니다.
* Hierarchy의 Canvas에 Prefab을 추가하신 후(Canvas가 없다면 새로 생성) Inspector에서 적절한 화면상의 위치와 크기를 설정해 주시면 됩니다.
    - UV Rect의 H값은 사전에 -1로 설정되어 있기 때문에 변경하실 필요가 없습니다.
* Multithreaded Rendering을 사용 중일 경우에는 정상적으로 작동하지 않기 때문에 빌드 시 'Player Settings' -> 'Other Settings'에서 Multithreaded Rendering 옵션을 해제해 주셔야 합니다.
<br/><br/>

## 플러그인 사용 방법
* 예시 프로젝트 내에 포함된 ExampleUsage.cs 스크립트 및 SampleScene을 통해 기본적인 사용 방법을 확인하실 수 있습니다.
* 빈 프로젝트 혹은 기존 프로젝트에서 패키지를 Import하여 사용하시고자 할 경우에는 아래 사항들을 참고해 주시기 바랍니다.
    - ARGlassController Prefab을 Scene에 추가하고 이미 Main Camera가 존재할 경우에는 이를 삭제합니다.
        + 혹은 Prefab을 사용하지 않고 직접 Scene의 Hierarchy에 빈 오브젝트를 생성한 후(혹은 기존 오브젝트를 사용하셔도 무방합니다) ARGlassController 컴포넌트를 추가할 수도 있습니다.
        + 이 경우 기존 Main Camera에 TrackedPoseDriver 컴포넌트를 추가합니다. (Unity에 기본 포함된 TrackedPoseDriver가 아닌 본 패키지에 포함된 스크립트를 추가해 주셔야 합니다.)
    - Inspector에서 ARGlassController 컴포넌트의 Target Device를 설정합니다.
    - 필요한 시점에 ARGlassController.Instance.Initialize를 호출하여 AR Glass를 초기화한 후 Frame 클래스를 통해 서비스에 필요한 정보(Pose, Gesture 등)를 획득합니다.
<br/><br/>

## 유의사항

### 프로젝트 빌드
* 일부 HW 모듈은 64비트 환경(ARM64), Android API Level 27 이상에서만 동작하기 때문에 APK로 빌드 시 'Player Settings' > 'Other Settings'에서 아래 항목들을 변경해 주시기 바랍니다.
    - Minimum API Level: Android 8.1 'Oreo' (API level 27) 혹은 그 이상
    - Scripting Backend: IL2CPP
    - Target Architectures: ARM64만 체크

### 실행 시
* 플러그인 내에서 SNPE를 사용하고 있는 관계로 현재 QC 단말에서만 작동이 가능합니다.
* 실행 전 단말의 지정된 경로에 SLAM 구동을 위한 Calibration 파일들이 위치해 있어야 합니다.
    - 경로: /sdcard/Samsung/Positioning/config/
        + AR Glass용 Calibration 파일: camera_arglasses_config.txt, imu_sensor_arglasses_config.txt
        + 스마트폰용의 경우 단말에 따라 Calibration 파일 구성이 다릅니다. 자세한 사항은 배포되는 패키지를 참고하여 주시기 바랍니다.
    - 경로: /sdcard/
        + brief_pattern.yml
* 시선 추적 플러그인을 사용할 경우 Adhawk Android, Adhawk Video Service, Calibration용 앱 등이 사전에 설치되어 있어야 합니다. (해당 앱이 설치되어 있지 않을 경우에도 통합 플러그인의 초기화 및 구동은 가능하나 시선 추적 결과가 정상적으로 출력되지 않습니다.)
* AR Glass를 연결하고 처음 초기화를 수행할 때 IMU, RGB 및 TOF 카메라에 대한 장치 접근 권한을 묻는 대화상자가 표시되고(카메라의 순서는 무작위로 바뀔 수 있습니다) 이들에 대한 접근 권한을 모두 허용하여 초기화가 정상적으로 완료되면 Frame 클래스 내의 각종 수치가 변화하는 것을 확인하실 수 있습니다.
    - 장치 접근 권한을 승인한 이후에는 AR Glass가 단말에 연결되어 있는 동안 다시 권한을 묻지 않고 한번에 초기화가 완료됩니다. 다만 AR Glass의 케이블을 분리하거나 전원을 제거할 경우에는 다음 연결 시에 다시 상기 방법대로 초기화를 수행하여 주셔야 합니다.
    - 간혹 초기화 시 앱이 중단될 경우에는 백그라운드에서 해당 앱을 완전히 Kill하신 후 다시 실행하여 주시기 바랍니다.
* 기타 문의사항이나 API에 관한 의견이 있으실 경우에는 아래 담당자를 통해 전달하여 주시기 바랍니다.
    - AR Lab. 김석열 (seokyeol.kim@samsung.com)